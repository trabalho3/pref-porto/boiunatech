﻿using Infraestrutura.Exceptions;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace TestesValidações
{
    public class InscriçãoHackathonInserir
    {
        private readonly IValidadorInscriçãoHackathonServiço _validadorHackathonServiço;
        private readonly IValidadorGeralServiço _validadorGeralServiço;

        public InscriçãoHackathonInserir()
        {
            _validadorGeralServiço = new ValidadorGeralServiço();
            _validadorHackathonServiço = new ValidadorInscriçãoHackathonServiço(_validadorGeralServiço);
        }

        [Theory]
        [ClassData(typeof(InscriçãoHackathonInserirDados))]
        public void ChecarValidaçãoException(DateTime nascimento, bool aceitou)
        {
            var mock = new Mock<CadastroModelo>();
            mock.Setup(c => c.Nascimento).Returns(nascimento);

            var inscriçãoErrada = new InscriçãoHackathonModelo
            {
                Cadastro = mock.Object,
                NomeEquipe = "Nome Qualquer",
                AceitouTermos = aceitou
            };

            Assert.Throws<ValidaçãoException>(() => _validadorHackathonServiço.Validar(inscriçãoErrada));
        }

        [Fact]
        public void ChecarInscriçãoCorreta()
        {
            var mock = new Mock<CadastroModelo>();
            mock.Setup(c => c.Nascimento).Returns(new DateTime(2001, 12, 5));

            var inscriçãoCorreta = new InscriçãoHackathonModelo
            {
                Cadastro = mock.Object,
                NomeEquipe = "Nome Qualquer",
                AceitouTermos = true
            };

            _validadorHackathonServiço.Validar(inscriçãoCorreta);

            Assert.True(true);
        }

    }

    public class InscriçãoHackathonInserirDados : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { DateTime.Now, true };
            yield return new object[] { new DateTime(2000, 06, 20), false };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
    }
}
