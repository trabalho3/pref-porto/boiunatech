using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Xunit;

namespace TestesValidações
{
    public class CadastroModeloValidaçãoTestes
    {
        private readonly ValidadorCadastroServiço _validadorCadastroServiço;
        private readonly IValidadorGeralServiço _validadorGeralServiço;

        public CadastroModeloValidaçãoTestes()
        {
            _validadorGeralServiço = new ValidadorGeralServiço();
            _validadorCadastroServiço = new ValidadorCadastroServiço(_validadorGeralServiço);
        }


        [Theory]
        [InlineData("", false)]
        [InlineData("                                           ", false)]
        [InlineData("E     u                     !", false)]
        [InlineData("Fulano Correto", true)]
        public void ChecarValidaçãoDoNomeDadoNomesEResultadosEsperados(string nome, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.NomeÉVálido(nome));
        }

        [Theory]
        [InlineData("535.146.590-60", true)]
        [InlineData("53514659060", true)]
        [InlineData("00000", false)]
        [InlineData("000.000.000-00", false)]
        [InlineData("00000000000", false)]
        public void ChecarValidaçãoDoCPFDadoCPFsEResultadosEsperados(string cpf, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.CPFÉVálido(cpf));
        }

        [Theory]
        [InlineData("(11) 78945-1236", true)]
        [InlineData("58745-89780", false)]
        [InlineData("(63) 9878-5546", false)]
        public void ChecarValidaçãoDoTelefoneDadoTelefonesEResultadosEsperados(string telefone, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.TelefoneÉVálido(telefone));
        }

        [Theory]
        [InlineData("correto@certo.ok", true)]
        [InlineData("correto.08@gmail.com", true)]
        [InlineData("correto-08@gmail.com", true)]
        [InlineData("correto_08@gmail.com", true)]
        [InlineData("corretocerto.ok", false)]
        [InlineData("Correto@correto.com", true)]
        [InlineData("certo@portonacional.to.gov.br", true)]
        [InlineData("errado@@portonacional.to.gov.br", false)]
        [InlineData("errado@portonacional..to.gov.br", false)]
        [InlineData("errado@portonacional.to.gov..br", false)]
        [InlineData("errado@portonacional.to..gov.br", false)]
        public void ChecarValidaçãoEmailDadoEmailsEResultadosEsperados(string email, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.EmailÉVálido(email));
        }

        [Theory]
        [InlineData("pluto", "pluto", false)]
        [InlineData("Teste123", "Teste123", true)]
        [InlineData("Teste123", "Teste124", false)]
        [InlineData("", "", false)]
        public void ChecarValidaçãoDasSenhasDadoSenhasEResultadosEsperados(string senha, string senhaConfirmação, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.SenhaÉVálida(senha, senhaConfirmação));
        }

        [Theory]
        [InlineData("00000-0000", false)]
        [InlineData("00000-000", true)]
        [InlineData("00000000", false)]
        [InlineData("0000", false)]
        public void ChecarValidaçãoDosCEPsDadoCEPsEResultadosEsperados(string cep, bool esperado)
        {
            Assert.Equal(esperado, _validadorCadastroServiço.CEPÉVálido(cep));
        }
    }
}
