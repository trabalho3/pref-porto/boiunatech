﻿using Infraestrutura.Exceptions;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Moq;
using System;
using Xunit;

namespace TestesValidações
{
    public class InscriçãoMinecraftInserir
    {
        private readonly IValidadorGeralServiço _validadorGeralServiço;
        private readonly IValidadorInscriçãoMinecraftServiço _validadorMinecraftServiço;

        public InscriçãoMinecraftInserir()
        {
            _validadorGeralServiço = new ValidadorGeralServiço();
            _validadorMinecraftServiço = new ValidadorInscriçãoMinecraftServiço(_validadorGeralServiço);
        }

        [Theory]
        [InlineData("", true)]
        [InlineData(" ", true)]
        [InlineData(null, true)]
        [InlineData("TesteCraft", true)]
        [InlineData("TesteCraft", false)]
        public void ChecarExceçãoDeValidação(string nick, bool aceitou)
        {
            var mock = new Mock<CadastroModelo>();
            mock.Setup(c => c.Nascimento).Returns(DateTime.Now);

            var inscriçãoErrada = new InscriçãoMinecraftModelo
            { 
                Cadastro = mock.Object, 
                Nickname = nick,
                AceitouTermos = aceitou
            };

            Assert.Throws<ValidaçãoException>(() => _validadorMinecraftServiço.Validar(inscriçãoErrada));
        }

        [Fact]
        public void ChecarInscriçãoCorreta()
        {
            var mock = new Mock<CadastroModelo>();
            mock.Setup(c => c.Nascimento).Returns(new DateTime(2000, 12, 1));

            var inscriçãoCerta = new InscriçãoMinecraftModelo
            {
                Cadastro = mock.Object,
                Nickname = "CorretoCraft",
                AceitouTermos = true
            };

            _validadorMinecraftServiço.Validar(inscriçãoCerta);

            Assert.True(true); //Chegando aqui é sucesso
        }
    }
}
