﻿using BoiunaSite.Controllers;
using Infraestrutura;
using Infraestrutura.Exceptions;
using Infraestrutura.Geradores;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons;
using Infraestrutura.Singletons.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.WebSockets;
using Xunit;

namespace TestesControllers
{
    public class CadastroModeloInserir
    {
        private readonly ICadastroGerador<CadastroModelo> _geradorCadastro;
        private readonly IDadosValidaçãoGerador _geradorDadosValidação;
        private readonly IUploadServiço _uploadServiço;
        private readonly IAmbienteSingleton _ambienteSingleton;
        private readonly IEmailServiço _emailServiço;
        private readonly IExtensõesUploadSingleton _extensõesUploads;
        private readonly IValidadorCadastroServiço _validadorCadastroServiço;
        private readonly ICriptografiaServiço _criptografiaServiço;
        private readonly IFormFile _arquivoMockado;

        public CadastroModeloInserir()
        {
            var uploadMock = new Mock<IUploadServiço>();
            var emailMock = new Mock<IEmailServiço>();
            var validadorGeral = new ValidadorGeralServiço();

            uploadMock.Setup(u => u.FazerUpload(It.IsAny<IFormFile>(), It.IsAny<string>(), It.IsAny<string[]>())).Returns("Mockado");

            _geradorCadastro = new CadastroGerador();
            _geradorDadosValidação = new DadosValidaçãoGerador();
            _uploadServiço = uploadMock.Object;
            _ambienteSingleton = new AmbienteDebugSingleton();
            _emailServiço = emailMock.Object;
            _extensõesUploads = new ExtensõesUploadSingleton();
            _validadorCadastroServiço = new ValidadorCadastroServiço(validadorGeral);
            _criptografiaServiço = new CriptografiaServiço();
            _arquivoMockado = null;

        }
        [Fact]
        public void VerificarSeOsEmailsSãoÚnicos()
        {
            var opções = new DbContextOptionsBuilder<Contexto>()
                .UseInMemoryDatabase("TestesEmailsBaseDeDados");

            using var contexto = new Contexto(opções.Options);
            var controller = RetornarController(contexto);

            var cadastroParaDarCerto = new CadastroViewModel
            {
                Nome = "Fulano 01 Correto",
                CPF = "197.235.060-92",
                Nascimento = new DateTime(2001, 6, 3),
                Telefone = "(00) 00000-0000",
                Email = "fulano@teste.com",
                Senha = "hello123",
                SenhaConfirmação = "hello123",
                Endereço = "Qualquer coisa",
                Cidade = "Teste",
                CEP = "00000-000",
                RGCPF = _arquivoMockado,
                ComprovanteResidência = _arquivoMockado,
                DeclaraçãoResidência = _arquivoMockado,
                Foto = _arquivoMockado,
                NomeBanco = "Banco Teste",
                Agência = "09855",
                Conta = "4578978",
                ComprovanteBancário = _arquivoMockado,
                NomeResponsável = null,
                CPFResponsável = null,
                RGCPFResponsável = null,
                FotoResponsável = null
            };

            var cadastroParaDarErro = new CadastroViewModel
            {
                Nome = "Fulano 02 Incorreto",
                CPF = "645.418.170-71",
                Nascimento = new DateTime(2001, 6, 3),
                Telefone = "(00) 00000-0002",
                Email = "fulano@teste.com",
                Senha = "erro123",
                SenhaConfirmação = "erro123",
                Endereço = "Qualquer coisa 02",
                Cidade = "Teste 02",
                CEP = "00000-002",
                RGCPF = _arquivoMockado,
                ComprovanteResidência = _arquivoMockado,
                DeclaraçãoResidência = _arquivoMockado,
                Foto = _arquivoMockado,
                NomeBanco = "Banco Teste",
                Agência = "09855",
                Conta = "4578978",
                ComprovanteBancário = _arquivoMockado,
                NomeResponsável = null,
                CPFResponsável = null,
                RGCPFResponsável = null,
                FotoResponsável = null
            };

            controller.Cadastrar(cadastroParaDarCerto);
            var resultado = controller.Cadastrar(cadastroParaDarErro);

            var viewResult = Assert.IsType<ViewResult>(resultado);
            Assert.Equal("Index", viewResult.ViewName);
        }

        [Fact]
        public void VerificarSeOsCPFsSãoÚnicos()
        {
            var opções = new DbContextOptionsBuilder<Contexto>()
                .UseInMemoryDatabase("TestesCPFsBaseDeDados");

            using var contexto = new Contexto(opções.Options);
            var controller = RetornarController(contexto);

            var cadastroParaDarCerto = new CadastroViewModel
            {
                Nome = "Fulano 01 Correto",
                CPF = "197.235.060-92",
                Nascimento = new DateTime(2001, 6, 3),
                Telefone = "(00) 00000-0000",
                Email = "fulano1@teste.com",
                Senha = "hello123",
                SenhaConfirmação = "hello123",
                Endereço = "Qualquer coisa",
                Cidade = "Teste",
                CEP = "00000-000",
                RGCPF = _arquivoMockado,
                ComprovanteResidência = _arquivoMockado,
                DeclaraçãoResidência = _arquivoMockado,
                Foto = _arquivoMockado,
                NomeBanco = "Banco Teste",
                Agência = "09855",
                Conta = "4578978",
                ComprovanteBancário = _arquivoMockado,
                NomeResponsável = null,
                CPFResponsável = null,
                RGCPFResponsável = null,
                FotoResponsável = null
            };

            var cadastroParaDarErro = new CadastroViewModel
            {
                Nome = "Fulano 02 Incorreto",
                CPF = "197.235.060-92",
                Nascimento = new DateTime(2001, 6, 3),
                Telefone = "(00) 00000-0002",
                Email = "fulano2@teste.com",
                Senha = "erro123",
                SenhaConfirmação = "erro123",
                Endereço = "Qualquer coisa 02",
                Cidade = "Teste 02",
                CEP = "00000-002",
                RGCPF = _arquivoMockado,
                ComprovanteResidência = _arquivoMockado,
                DeclaraçãoResidência = _arquivoMockado,
                Foto = _arquivoMockado,
                NomeBanco = "Banco Teste",
                Agência = "09855",
                Conta = "4578978",
                ComprovanteBancário = _arquivoMockado,
                NomeResponsável = null,
                CPFResponsável = null,
                RGCPFResponsável = null,
                FotoResponsável = null
            };

            controller.Cadastrar(cadastroParaDarCerto);
            var resultado = controller.Cadastrar(cadastroParaDarErro);

            var viewResult = Assert.IsType<ViewResult>(resultado);
            Assert.Equal("Index", viewResult.ViewName);
        }

        [Theory]
        [ClassData(typeof(VerificarValidaçãoExceptionDados))]
        public void VerificarValidaçãoException(CadastroViewModel cadastroParaDarErro)
        {
            var opções = new DbContextOptionsBuilder<Contexto>()
                .UseInMemoryDatabase("TestesValidaçãoExceptionBaseDeDados");
            using var contexto = new Contexto(opções.Options);
            var controller = RetornarController(contexto);

            var resultado = controller.Cadastrar(cadastroParaDarErro);

            var viewResult = Assert.IsType<ViewResult>(resultado);
            Assert.Equal("Index", viewResult.ViewName);
        }

        [Theory]
        [ClassData(typeof(VerificarCadastrosCorretosDados))]
        public void VerificarCadastrosCorretos(CadastroViewModel cadastroParaDarCerto)
        {
            var opções = new DbContextOptionsBuilder<Contexto>()
                .UseInMemoryDatabase("TestesCadastrosCorretosBaseDeDados");
            using var contexto = new Contexto(opções.Options);
            var controller = RetornarController(contexto);

            var resultado = controller.Cadastrar(cadastroParaDarCerto);

            var viewResult = Assert.IsType<ViewResult>(resultado);
            Assert.Equal("Sucesso", viewResult.ViewName);
        }

        private CadastroController RetornarController(Contexto contexto)
        {
            return new CadastroController(contexto, _geradorCadastro, _geradorDadosValidação, _uploadServiço,
                _ambienteSingleton, _emailServiço, _extensõesUploads, _validadorCadastroServiço, _criptografiaServiço);
        }
    }


    public class VerificarValidaçãoExceptionDados : IEnumerable<object[]>
    {
        private readonly IFormFile _arquivoMockado = null;
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano 02 CPF Incorreto",
                    CPF = "000.000.000-00",
                    Nascimento = new DateTime(2001, 6, 3),
                    Telefone = "(00) 00000-0002",
                    Email = "fulano2@teste.com",
                    Senha = "erro123",
                    SenhaConfirmação = "erro123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "09855",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado,
                    NomeResponsável = null,
                    CPFResponsável = null,
                    RGCPFResponsável = null,
                    FotoResponsável = null
                }
            };

            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano Com Agência Errada",
                    CPF = "456.995.740-42",
                    Nascimento = new DateTime(2001, 6, 3),
                    Telefone = "(00) 00000-0002",
                    Email = "fulano4@teste.com",
                    Senha = "erro123",
                    SenhaConfirmação = "erro123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "098s55",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado,
                    NomeResponsável = null,
                    CPFResponsável = null,
                    RGCPFResponsável = null,
                    FotoResponsável = null
                }
            };

            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano sem os dados do responsável",
                    CPF = "853.468.670-07",
                    Nascimento = DateTime.Now,
                    Telefone = "(00) 00000-0002",
                    Email = "fulano5@teste.com",
                    Senha = "erro123",
                    SenhaConfirmação = "erro123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "09855",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado
                }
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }


    public class VerificarCadastrosCorretosDados : IEnumerable<object[]>
    {
        private readonly IFormFile _arquivoMockado = null;
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano correto",
                    CPF = "456.995.740-42",
                    Nascimento = new DateTime(2000, 12, 5),
                    Telefone = "(00) 00000-0002",
                    Email = "fulano1@portonacional.to.gov.br",
                    Senha = "certo123",
                    SenhaConfirmação = "certo123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "09855",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado,
                    NomeResponsável = null,
                    CPFResponsável = null,
                    RGCPFResponsável = null,
                    FotoResponsável = null
                }
            };

            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano correto",
                    CPF = "853.468.670-07",
                    Nascimento = new DateTime(2000, 12, 5),
                    Telefone = "(00) 00000-0002",
                    Email = "fulano2@portonacional.to.gov.br",
                    Senha = "certo123",
                    SenhaConfirmação = "certo123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "09855",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado,
                    NomeResponsável = null,
                    CPFResponsável = null,
                    RGCPFResponsável = null,
                    FotoResponsável = null
                }
            };

            yield return new object[]
            {
                new CadastroViewModel
                {
                    Nome = "Fulano correto",
                    CPF = "197.235.060-92",
                    Nascimento = new DateTime(2009, 12, 5),
                    Telefone = "(00) 00000-0002",
                    Email = "fulano5@portonacional.to.gov.br",
                    Senha = "certo123",
                    SenhaConfirmação = "certo123",
                    Endereço = "Qualquer coisa 02",
                    Cidade = "Teste 02",
                    CEP = "00000-002",
                    RGCPF = _arquivoMockado,
                    ComprovanteResidência = _arquivoMockado,
                    DeclaraçãoResidência = _arquivoMockado,
                    Foto = _arquivoMockado,
                    NomeBanco = "Banco Teste",
                    Agência = "09855",
                    Conta = "4578978",
                    ComprovanteBancário = _arquivoMockado,
                    NomeResponsável = "Aloha Testando Correto",
                    CPFResponsável = "853.468.670-07",
                    RGCPFResponsável = _arquivoMockado,
                    FotoResponsável = _arquivoMockado
                }
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        
    }


}
