﻿using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons;
using Infraestrutura.Singletons.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace TestesHelpers
{
    public class UploadHelperTeste
    {
        [Fact]
        public void FazerUploadDoArquivoDeTeste()
        {
#if DEBUG
            IAmbienteSingleton ambiente = new AmbienteDebugSingleton();
#elif DOCKERDEBUG
            IAmbienteSingleton ambiente = new AmbienteDebugSingleton();
#else
            IAmbienteSingleton ambiente = new AmbienteReleaseSingleton();
#endif

            IExtensõesUploadSingleton extensõesUpload = new ExtensõesUploadSingleton();
            ICriptografiaServiço criptografia = new CriptografiaServiço();

            var helper = new UploadServiço(ambiente, criptografia);
            var arquivo = helper.FazerUpload(new ArquivoUploadFalso(), ambiente.PastaRGCPF, extensõesUpload.ExtensõesPDF);

            Assert.True(File.Exists(arquivo));

            File.Delete(arquivo); //É bom deixar o sistema limpo
        }
    }

    public class ArquivoUploadFalso : IFormFile
    {
        private readonly Stream _stream;

        public ArquivoUploadFalso()
        {
            var assembly = Assembly.GetExecutingAssembly();
            _stream = assembly.GetManifestResourceStream("TestesHelpers.TesteUpload.pdf");
        }

        public string ContentType => throw new NotImplementedException();

        public string ContentDisposition => throw new NotImplementedException();

        public IHeaderDictionary Headers => throw new NotImplementedException();

        public long Length => _stream.Length;

        public string Name => throw new NotImplementedException();

        public string FileName => "Testes Upload.pdf";

        public void CopyTo(Stream target) => _stream.CopyTo(target);

        public Task CopyToAsync(Stream target, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Stream OpenReadStream()
        {
            throw new NotImplementedException();
        }
    }
}
