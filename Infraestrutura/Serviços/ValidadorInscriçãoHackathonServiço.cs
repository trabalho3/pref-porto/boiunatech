﻿using Infraestrutura.Exceptions;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Interfaces;
using System;

namespace Infraestrutura.Serviços
{
    public class ValidadorInscriçãoHackathonServiço : IValidadorInscriçãoHackathonServiço
    {
        private readonly IValidadorGeralServiço _validadorGeral;

        public ValidadorInscriçãoHackathonServiço(IValidadorGeralServiço validadorGeral)
        {
            _validadorGeral = validadorGeral;
        }

        public void Validar<T>(IInscriçãoHackathonModelo<T> inscrição) where T : ICadastroModelo
        {
            if (!VerificarValidadeNomeEquipe(inscrição.NomeEquipe)) throw new ValidaçãoException("Nome da Equipe");
            if (!inscrição.AceitouTermos) throw new ValidaçãoException("Aceitar Termos");
            if (!VerificarValidadeNascimento(inscrição.Cadastro.Nascimento)) throw new ValidaçãoException("Menor que 16 anos");
        }

        private bool VerificarValidadeNascimento(DateTime nascimento) => _validadorGeral.VerificarIdadeMínimaAtingida(16, nascimento);
        private bool VerificarValidadeNomeEquipe(string nome) => _validadorGeral.VerificarStringNãoVazia(nome);
    }
}
