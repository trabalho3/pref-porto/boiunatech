﻿using Infraestrutura.Exceptions;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços
{
    public class ValidadorRedefinirSenhaViewModelServiço : IValidadorRedefinirSenhaViewModelServiço
    {
        private readonly IValidadorGeralServiço _validadorGeral;

        public ValidadorRedefinirSenhaViewModelServiço(IValidadorGeralServiço validadorGeral)
        {
            _validadorGeral = validadorGeral;
        }

        public void ValidarDadosInseridos(IRedefinirSenhaViewModel dados)
        {
            if (dados.Senha != dados.ConfirmarSenha) throw new ValidaçãoException("Senhas não conferem");
            if (!_validadorGeral.VerificarStringNãoVazia(dados.Senha)) throw new ValidaçãoException("Senha");
        }
    }
}
