﻿using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;
using System;

namespace Infraestrutura.Serviços
{
    public class EmailServiço : IEmailServiço
    {
        private readonly string _charSet = "123456.,+-*qwertyuiopsdfghjklçzxcvbnmQWERTYUIOPASDFGHJKLÇZXCVBNM";
        private readonly IAmbienteSingleton _ambiente;

        public EmailServiço(IAmbienteSingleton ambiente)
        {
            _ambiente = ambiente;
        }

        public void EmailConfirmação(string token, string email)
        {
            var mensagem = PreprararMensagemConfirmação(token, email);
            EnviarEmail(mensagem);
        }


        public void ReenviarEmail(string token, string email)
        {
            var mensagem = PreprararMensagemReenvio(token, email);
            EnviarEmail(mensagem);
        }

        public string GerarToken() => $"{GerarStringAleatória(40)}";

        public void EnviarRecuperarSenha(string token, string email)
        {
            var mensagem = PreprararMensagemRecuperar(token, email);
            EnviarEmail(mensagem);
        }

        private void EnviarEmail(MimeMessage mensagem)
        {
            SmtpClient smtpClient = new SmtpClient();

            smtpClient.Connect(_ambiente.ServidorEmail, _ambiente.ServidorEmailPorta, true);
            smtpClient.Authenticate(_ambiente.UsuárioServidorEmail, _ambiente.SenhaServidorEmail);
            smtpClient.Send(mensagem);
            smtpClient.Disconnect(true);
        }

        private MimeMessage IniciarEmail(string emailAlvo)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(_ambiente.UsuárioServidorEmail));
            mimeMessage.To.Add(new MailboxAddress(emailAlvo));
            mimeMessage.To.Add(new MailboxAddress(_ambiente.UsuárioServidorEmail));

            return mimeMessage;

        }

        private MimeMessage PreprararMensagemReenvio(string token, string email)
        {
            var mimeMessage = IniciarEmail(email);
            mimeMessage.Subject = "Confirme seu cadastro";
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = TextoEmailToken(token)
            };

            return mimeMessage;
        }

        private MimeMessage PreprararMensagemConfirmação(string token, string email)
        {
            var mimeMessage = IniciarEmail(email);
            mimeMessage.Subject = "Confirme seu cadastro";
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = TextoEmailToken(token)
            };

            return mimeMessage;
        }
        private MimeMessage PreprararMensagemRecuperar(string token, string email)
        {
            var mimeMessage = IniciarEmail(email);
            mimeMessage.Subject = "Recuperar Senha";
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = TextoRedefinirSenhaToken(token)
            };

            return mimeMessage;
        }

        private string GerarStringAleatória(int tamanho)
        {
            var retorno = "";
            var random = new Random();

            for (int i = 0; i < tamanho; i++)
            {
                var índice = random.Next(_charSet.Length);
                retorno = $"{retorno}{_charSet[índice]}";
            }

            return retorno;
        }

        private static string TextoRedefinirSenhaToken(string token)
        {
            return "<p>Redefinição.</p><br>" +
                   $"<p>Por favor, redefina sua senha clicando <a href='https://boiuna.portonacional.to.gov.br/EsqueciSenha/Redefinir/{token}'>aqui</a> ou " +
                   $"colando o seguinte endereço na sua barra de navegação: https://boiuna.portonacional.to.gov.br/EsqueciSenha/Redefinir/{token} </p>";
        }

        private static string TextoEmailToken(string token)
        {
            return "<p>Conforme solicitado, enviamos seu código de confirmação.</p><br>" +
                   $"Por favor, confirme seu e-mail clicando <a href='https://boiuna.portonacional.to.gov.br/ConfirmarEmail/Token/{token}'>aqui</a> ou " +
                   $"colando o seguinte endereço na sua barra de navegação: https://boiuna.portonacional.to.gov.br/ConfirmarEmail/Token/{token}";
        }
    }
}
