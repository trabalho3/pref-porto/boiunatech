﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IEmailServiço
    {
        void EmailConfirmação(string token, string email);
        void ReenviarEmail(string token, string email);
        void EnviarRecuperarSenha(string token, string email);
        string GerarToken();
    }
}
