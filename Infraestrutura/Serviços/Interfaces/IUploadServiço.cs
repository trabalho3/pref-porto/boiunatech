﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IUploadServiço
    {
        string FazerUpload(IFormFile arquivo, string pasta, string[] extensões);
        void DeletarUpload(string caminhoCompleto);
        bool ArquivoNulo(IFormFile arquivo);
    }
}
