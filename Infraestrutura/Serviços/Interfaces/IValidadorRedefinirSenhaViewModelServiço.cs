﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorRedefinirSenhaViewModelServiço
    {
        void ValidarDadosInseridos(IRedefinirSenhaViewModel dados);
    }
}
