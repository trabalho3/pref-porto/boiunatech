﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface ICriptografiaServiço
    {
        string Criptografar(string dado);
        bool CompararCriptografado(string dadoCriptografado, string dadoBruto);
    }
}
