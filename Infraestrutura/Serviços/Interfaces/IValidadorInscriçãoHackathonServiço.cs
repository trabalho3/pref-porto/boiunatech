﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorInscriçãoHackathonServiço
    {

        void Validar<T>(IInscriçãoHackathonModelo<T> inscrição) where T : ICadastroModelo;
    }
}
