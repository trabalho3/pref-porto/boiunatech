﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorGeralServiço
    {
        bool VerificarStringNãoVazia(string dado);
        bool VerificarIdadeMínimaAtingida(int idade, DateTime nascimento);
        bool VerificarTemCorrespondênciaRegex(string dado, string regex);
    }
}
