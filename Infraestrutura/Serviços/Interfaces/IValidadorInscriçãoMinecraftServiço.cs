﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorInscriçãoMinecraftServiço
    {

        void Validar<T>(IInscriçãoMinecraftModelo<T> inscrição) where T : ICadastroModelo;
    }
}
