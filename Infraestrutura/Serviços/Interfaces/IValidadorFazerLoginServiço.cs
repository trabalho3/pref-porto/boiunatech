﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorFazerLoginServiço
    {
        void ValidarDadosInseridos(string email, string senha);
        void ValidarCorrespondência(ICadastroModelo cadastro);
    }
}
