﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.Serviços.Interfaces
{
    public interface IValidadorCadastroServiço
    {
        public void Validar(ICadastroModelo cadastro);
    }
}
