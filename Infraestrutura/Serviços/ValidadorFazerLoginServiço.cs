﻿using Infraestrutura.Exceptions;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Serviços
{
    public class ValidadorFazerLoginServiço : IValidadorFazerLoginServiço
    {
        private readonly IValidadorGeralServiço _validadorGeralServiço;

        public ValidadorFazerLoginServiço(IValidadorGeralServiço validadorGeralServiço)
        {
            _validadorGeralServiço = validadorGeralServiço;
        }

        public void ValidarCorrespondência(ICadastroModelo cadastro)
        {
            if (cadastro == null) throw new ValidaçãoException("Usuário não encontrado");
            if (!cadastro.Confirmado) throw new ValidaçãoException("E-mail não confirmado");
        }

        public void ValidarDadosInseridos(string email, string senha)
        {
            if (!_validadorGeralServiço.VerificarStringNãoVazia(email)) throw new ValidaçãoException("E-mail");
            if (!_validadorGeralServiço.VerificarStringNãoVazia(senha)) throw new ValidaçãoException("Senha");
        }
    }
}
