﻿using Infraestrutura.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Infraestrutura.Serviços
{
    public class CriptografiaServiço : ICriptografiaServiço
    {
        public string Criptografar(string texto)
        {
            byte[] dados = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(texto));
            StringBuilder sBuilder = new StringBuilder();

            foreach (var dado in dados)
            {
                sBuilder.Append(dado.ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public bool CompararCriptografado(string md5, string texto)
        {
            return Criptografar(texto) == md5;
        }
    }
}
