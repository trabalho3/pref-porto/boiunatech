﻿using Infraestrutura.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Infraestrutura.Serviços
{
    public class ValidadorGeralServiço : IValidadorGeralServiço
    {
        public bool VerificarIdadeMínimaAtingida(int idade, DateTime nascimento) => nascimento.AddYears(idade) <= DateTime.Now;

        public bool VerificarStringNãoVazia(string dado) => (!string.IsNullOrEmpty(dado) && !string.IsNullOrWhiteSpace(dado));

        public bool VerificarTemCorrespondênciaRegex(string dado, string regex)
        {
            var regexObjeto = new Regex(regex);

            return regexObjeto.Match(dado).Success;
        }
    }
}
