﻿using Infraestrutura.Exceptions;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace Infraestrutura.Serviços
{
    public class UploadServiço : IUploadServiço
    {
        private readonly IAmbienteSingleton _ambiente;
        private readonly ICriptografiaServiço _criptografia;

        public UploadServiço(IAmbienteSingleton ambiente, ICriptografiaServiço criptografia)
        {
            _ambiente = ambiente;
            _criptografia = criptografia;
        }

        public bool ArquivoNulo(IFormFile arquivo)
        {
            return (arquivo == null || arquivo.Length == 0);
        }

        public void DeletarUpload(string caminhoCompleto)
        {
            if(!string.IsNullOrEmpty(caminhoCompleto) && File.Exists(caminhoCompleto)) File.Delete(caminhoCompleto);
        }

        public string FazerUpload (IFormFile arquivo, string pasta, string[] extensões)
        {
            if (ArquivoNulo(arquivo)) throw new ArquivoNuloException();
            foreach (var extensão in extensões)
            {
                if(ChecarExtensão(arquivo, extensão))
                {
                    return PrepararUploadExtensão(arquivo, pasta, extensão);
                }
            }
            throw new ExtensãoInválidaException();
        }
        private string PrepararUploadExtensão(IFormFile arquivo, string pasta, string extensão)
        {
            if (!Directory.Exists($"{_ambiente.CaminhoUpload}/{pasta}")) Directory.CreateDirectory($"{_ambiente.CaminhoUpload}/{pasta}");

            var nomeArquivoDestino = $"{_criptografia.Criptografar(arquivo.FileName)}{DateTime.Now.Millisecond}";
            var caminhoArquivoDestino = $"{_ambiente.CaminhoUpload}/{pasta}";

            while (File.Exists($"{caminhoArquivoDestino}/{nomeArquivoDestino}{extensão}"))
            {
                nomeArquivoDestino = $"{nomeArquivoDestino}1";
            }

            var arquivoDestino = $"{caminhoArquivoDestino}/{nomeArquivoDestino}{extensão}";
            using var stream = new FileStream(arquivoDestino, FileMode.Create);
            arquivo.CopyTo(stream);

            return arquivoDestino;
        }

        private bool ChecarExtensão(IFormFile arquivo, string extensão)
        {
            if (!arquivo.FileName.Contains(extensão)) return false;
            return true;
        }
    }
}
