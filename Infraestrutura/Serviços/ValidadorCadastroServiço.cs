﻿using Brazil.Data.Validators;
using Infraestrutura.Exceptions;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Interfaces;
using System;

namespace Infraestrutura.Serviços
{
    public class ValidadorCadastroServiço : IValidadorCadastroServiço
    {
        private readonly IValidadorGeralServiço _validadorGeral;

        public ValidadorCadastroServiço(IValidadorGeralServiço validadorGeral)
        {
            _validadorGeral = validadorGeral;
        }


        public void Validar(ICadastroModelo cadastro)
        {
            if (!NascimentoÉVálido(cadastro.Nascimento)) throw new ValidaçãoException("Menor que 10 anos");
            if (!NomeÉVálido(cadastro.Nome)) throw new ValidaçãoException("Nome");
            if (!CPFÉVálido(cadastro.CPF)) throw new ValidaçãoException("CPF");
            if (!TelefoneÉVálido(cadastro.Telefone)) throw new ValidaçãoException("Telefone");
            if (!EmailÉVálido(cadastro.Email)) throw new ValidaçãoException("E-mail");
            if (!SenhaÉVálida(cadastro.Senha, cadastro.SenhaConfirmação)) throw new ValidaçãoException("Senha");
            if (!EndereçoÉVálido(cadastro.Endereço)) throw new ValidaçãoException("Endereço");
            if (!CidadeÉVálida(cadastro.Cidade)) throw new ValidaçãoException("Cidade");
            if (!CEPÉVálido(cadastro.CEP)) throw new ValidaçãoException("CEP");
            if (!_validadorGeral.VerificarStringNãoVazia(cadastro.CaminhoComprovanteBancário)) throw new ValidaçãoException("Comprovante Bancário");
            if (!_validadorGeral.VerificarStringNãoVazia(cadastro.CaminhoRGCPF)) throw new ValidaçãoException("Cópia do RG e CPF");
            if (!_validadorGeral.VerificarStringNãoVazia(cadastro.CaminhoComprovanteEndereço)) throw new ValidaçãoException("Comprovante de Endereço");
            if (!_validadorGeral.VerificarStringNãoVazia(cadastro.CaminhoFoto)) throw new ValidaçãoException("Selfie com RG e CPF");
            if (!_validadorGeral.VerificarStringNãoVazia(cadastro.NomeBanco)) throw new ValidaçãoException("Nome do Banco");
            if (!AgênciaÉVálida(cadastro.Agência)) throw new ValidaçãoException("Agência");
            if (!ContaÉVálida(cadastro.Conta)) throw new ValidaçãoException("Conta Bancária");
            if (!NomeResponsávelÉVálido(cadastro.NomeResponsável, cadastro.Nascimento)) throw new ValidaçãoException("Nome do Responsável");
            if (!CPFResponsávelÉVálido(cadastro.CPFResponsável, cadastro.Nascimento)) throw new ValidaçãoException("CPF do Responsável");
            if (!RGCPFReponsávelÉVálido(cadastro.CaminhoRGCPFResponsável, cadastro.Nascimento)) throw new ValidaçãoException("RG e CPF do Responsável");
            if (!FotoResponsávelÉVálida(cadastro.CaminhoFotoResponsável, cadastro.Nascimento)) throw new ValidaçãoException("Selfie do Responsável com RG e CPF");
        }

        public bool NomeÉVálido(string nome)
        {
            return (_validadorGeral.VerificarStringNãoVazia(nome) && nome.Replace(" ", "").Length > 10);
        }

        public bool SenhaÉVálida(string senha, string senhaConfirmação)
        {
            if (senha != senhaConfirmação) return false;

            return (!string.IsNullOrEmpty(senha) && senha.Length > 6);
        }
        public bool CPFÉVálido(string cpf) => CpfValidator.Validate(cpf);
        public bool TelefoneÉVálido(string telefone) => _validadorGeral.VerificarTemCorrespondênciaRegex(telefone, "^[(][0-9]{2}[)] [0-9]{5}[-][0-9]{4}$");
        public bool EmailÉVálido(string email) => (_validadorGeral.VerificarTemCorrespondênciaRegex(email, "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"));
        public bool EndereçoÉVálido(string endereço) => _validadorGeral.VerificarStringNãoVazia(endereço);
        public bool CidadeÉVálida(string cidade) => _validadorGeral.VerificarStringNãoVazia(cidade);
        public bool CEPÉVálido(string cep) => _validadorGeral.VerificarTemCorrespondênciaRegex(cep, "^[0-9]{5}[-][0-9]{3}$");
        public bool AgênciaÉVálida(string agência) => _validadorGeral.VerificarTemCorrespondênciaRegex(agência, "^[0-9]+$");
        public bool ContaÉVálida(string conta) => _validadorGeral.VerificarTemCorrespondênciaRegex(conta, "^[0-9]+$");
        public bool NomeResponsávelÉVálido(string nomeResponsável, DateTime nascimento)
        {
            if (MaiorDeIdade(nascimento)) return true;

            return NomeÉVálido(nomeResponsável);
        }
        public bool CPFResponsávelÉVálido(string CPFResponsável, DateTime nascimento)
        {
            if (MaiorDeIdade(nascimento)) return true;

            return CPFÉVálido(CPFResponsável);
        }
        public bool RGCPFReponsávelÉVálido(string caminhoRGCPFResponsável, DateTime nascimento)
        {
            if (MaiorDeIdade(nascimento)) return true;

            return _validadorGeral.VerificarStringNãoVazia(caminhoRGCPFResponsável);
        }
        public bool NascimentoÉVálido(DateTime nascimento) =>_validadorGeral.VerificarIdadeMínimaAtingida(10, nascimento);
        public bool FotoResponsávelÉVálida(string caminhoFotoResponsável, DateTime nascimento)
        {
            if (MaiorDeIdade(nascimento)) return true;

            return _validadorGeral.VerificarStringNãoVazia(caminhoFotoResponsável);
        }

        private bool MaiorDeIdade(DateTime nascimento) => _validadorGeral.VerificarIdadeMínimaAtingida(18, nascimento);
    }
}
