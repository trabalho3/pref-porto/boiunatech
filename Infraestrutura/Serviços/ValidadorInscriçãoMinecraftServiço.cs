﻿using Infraestrutura.Exceptions;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Interfaces;
using System;

namespace Infraestrutura.Serviços
{
    public class ValidadorInscriçãoMinecraftServiço : IValidadorInscriçãoMinecraftServiço
    {
        private readonly IValidadorGeralServiço _validadorGeral;

        public ValidadorInscriçãoMinecraftServiço(IValidadorGeralServiço validadorGeral)
        {
            _validadorGeral = validadorGeral;
        }

        public void Validar<T>(IInscriçãoMinecraftModelo<T> inscrição) where T : ICadastroModelo
        {
            if (!VerificarNickname(inscrição.Nickname)) throw new ValidaçãoException("Nickname");
            if (!inscrição.AceitouTermos) throw new ValidaçãoException("Aceitar Termos");
            if (!VerificarNascimento(inscrição.Cadastro.Nascimento)) throw new ValidaçãoException("Menor que 10 anos");
        }

        private bool VerificarNascimento(DateTime nascimento) => _validadorGeral.VerificarIdadeMínimaAtingida(10, nascimento);
        private bool VerificarNickname(string nickname) => _validadorGeral.VerificarStringNãoVazia(nickname);
    }
}
