﻿using Infraestrutura.Modelos;
using Infraestrutura.Serviços;
using Infraestrutura.Serviços.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Infraestrutura
{
    public class Contexto : DbContext
    {

        private ICriptografiaServiço _criptografiaServiço;
        private EnvironmentVariableTarget _alvoAmbiente;

        public DbSet<CadastroModelo> Cadastros { get; set; }
        public DbSet<InscriçãoMinecraftModelo> InscriçõesMinecraft { get; set; }
        public DbSet<InscriçãoHackathonModelo> InscriçõesHackathon { get; set; }
        public DbSet<LoginModelo> Admin { get; set; }
        public DbSet<RecuperarSenhasTokenModelo> RecuperarSenhas { get; set; }

        public Contexto([NotNullAttribute] DbContextOptions options) : base(options)
        {
            CriarCriptografiaServiço();
            ConfigurarAmbiente();
        }

        public Contexto()
        {
            CriarCriptografiaServiço();
            ConfigurarAmbiente();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (optionsBuilder.IsConfigured) return;

            var servidor = Environment.GetEnvironmentVariable("Servidor", _alvoAmbiente);
            var usuário = Environment.GetEnvironmentVariable("Usuario", _alvoAmbiente);
            var senha = Environment.GetEnvironmentVariable("SenhaMySQLBoiuna", _alvoAmbiente);
#if DOCKERDEBUG
            var bancoDeDados = "boiuna_dev";
#else
            var bancoDeDados = Environment.GetEnvironmentVariable("BancoDeDados", _alvoAmbiente);
#endif
            optionsBuilder.UseMySql($"Server={servidor};User Id={usuário};Password={senha};Database={bancoDeDados}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var senha = Environment.GetEnvironmentVariable("SenhaAdminBoiuna", _alvoAmbiente);
            var login = Environment.GetEnvironmentVariable("LoginAdminBoiuna", _alvoAmbiente);


            modelBuilder.Entity<CadastroModelo>().HasKey(c => c.Id);

            modelBuilder.Entity<InscriçãoMinecraftModelo>().HasKey(m => m.Id);
            modelBuilder.Entity<InscriçãoMinecraftModelo>().HasOne(m => m.Cadastro);

            modelBuilder.Entity<InscriçãoHackathonModelo>().HasKey(h => h.Id);
            modelBuilder.Entity<InscriçãoHackathonModelo>().HasOne(h => h.Cadastro);

            modelBuilder.Entity<LoginModelo>().HasKey(l => l.Id);
            modelBuilder.Entity<LoginModelo>().HasData(new LoginModelo { Nome = "Boiuna Master", Senha = _criptografiaServiço.Criptografar(senha), Login = login, Id = 1 });

            modelBuilder.Entity<RecuperarSenhasTokenModelo>().HasKey(r => r.Id);
            modelBuilder.Entity<RecuperarSenhasTokenModelo>().HasOne(r => r.Cadastro);

        }

        private void ConfigurarAmbiente()
        {
#if DEBUG
            _alvoAmbiente = EnvironmentVariableTarget.User;
#else
            _alvoAmbiente = EnvironmentVariableTarget.Process;
#endif
        }

        private void CriarCriptografiaServiço()
        {
            _criptografiaServiço = new CriptografiaServiço();
        }
    }
}
