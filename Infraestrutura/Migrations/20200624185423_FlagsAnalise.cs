﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestrutura.Migrations
{
    public partial class FlagsAnalise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Análise",
                table: "InscriçõesMinecraft",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Aprovada",
                table: "InscriçõesMinecraft",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Análise",
                table: "InscriçõesMinecraft");

            migrationBuilder.DropColumn(
                name: "Aprovada",
                table: "InscriçõesMinecraft");
        }
    }
}
