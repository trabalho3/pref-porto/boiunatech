﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestrutura.Migrations
{
    public partial class InscricoesHackathon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InscriçõesHackathon",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Aprovada = table.Column<bool>(nullable: false),
                    Análise = table.Column<bool>(nullable: false),
                    CadastroId = table.Column<int>(nullable: true),
                    NomeEquipe = table.Column<string>(nullable: true),
                    Membros = table.Column<string>(nullable: true),
                    AceitouTermos = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InscriçõesHackathon", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InscriçõesHackathon_Cadastros_CadastroId",
                        column: x => x.CadastroId,
                        principalTable: "Cadastros",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InscriçõesHackathon_CadastroId",
                table: "InscriçõesHackathon",
                column: "CadastroId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InscriçõesHackathon");
        }
    }
}
