﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestrutura.Migrations
{
    public partial class Fotos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CaminhoFoto",
                table: "Cadastros",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "CaminhoFotoResponsável",
                table: "Cadastros",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CaminhoFoto",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "CaminhoFotoResponsável",
                table: "Cadastros");
        }
    }
}
