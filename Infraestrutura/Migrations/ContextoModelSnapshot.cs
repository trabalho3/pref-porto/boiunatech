﻿// <auto-generated />
using System;
using Infraestrutura;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infraestrutura.Migrations
{
    [DbContext(typeof(Contexto))]
    partial class ContextoModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Infraestrutura.Modelos.CadastroModelo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Agência")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CEP")
                        .IsRequired()
                        .HasColumnType("char(9)");

                    b.Property<string>("CPF")
                        .IsRequired()
                        .HasColumnType("char(14)");

                    b.Property<string>("CPFResponsável")
                        .HasColumnType("char(14)");

                    b.Property<string>("CaminhoComprovanteBancário")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoComprovanteEndereço")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoDeclaraçãoDeResidência")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoFoto")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoFotoResponsável")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoRGCPF")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("CaminhoRGCPFResponsável")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Cidade")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<bool>("Confirmado")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("Conta")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Endereço")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<DateTime>("Nascimento")
                        .HasColumnType("date");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NomeBanco")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NomeResponsável")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Senha")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Telefone")
                        .IsRequired()
                        .HasColumnType("char(15)");

                    b.Property<string>("TokenConfirmação")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("Cadastros");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.InscriçãoHackathonModelo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<bool>("AceitouTermos")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool>("Análise")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool>("Aprovada")
                        .HasColumnType("tinyint(1)");

                    b.Property<int?>("CadastroId")
                        .HasColumnType("int");

                    b.Property<string>("Membros")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NomeEquipe")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.HasIndex("CadastroId");

                    b.ToTable("InscriçõesHackathon");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.InscriçãoMinecraftModelo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<bool>("AceitouTermos")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool>("Análise")
                        .HasColumnType("tinyint(1)");

                    b.Property<bool>("Aprovada")
                        .HasColumnType("tinyint(1)");

                    b.Property<int?>("CadastroId")
                        .HasColumnType("int");

                    b.Property<string>("Nickname")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.HasIndex("CadastroId");

                    b.ToTable("InscriçõesMinecraft");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.LoginModelo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Senha")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("Admin");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Login = "boiunamaster",
                            Nome = "Boiuna Master",
                            Senha = "f9a6448c57e630ee049772869695c973"
                        });
                });

            modelBuilder.Entity("Infraestrutura.Modelos.RecuperarSenhasTokenModelo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("CadastroId")
                        .HasColumnType("int");

                    b.Property<string>("Token")
                        .IsRequired()
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<bool>("Usado")
                        .HasColumnType("tinyint(1)");

                    b.HasKey("Id");

                    b.HasIndex("CadastroId");

                    b.ToTable("RecuperarSenhas");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.InscriçãoHackathonModelo", b =>
                {
                    b.HasOne("Infraestrutura.Modelos.CadastroModelo", "Cadastro")
                        .WithMany()
                        .HasForeignKey("CadastroId");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.InscriçãoMinecraftModelo", b =>
                {
                    b.HasOne("Infraestrutura.Modelos.CadastroModelo", "Cadastro")
                        .WithMany()
                        .HasForeignKey("CadastroId");
                });

            modelBuilder.Entity("Infraestrutura.Modelos.RecuperarSenhasTokenModelo", b =>
                {
                    b.HasOne("Infraestrutura.Modelos.CadastroModelo", "Cadastro")
                        .WithMany()
                        .HasForeignKey("CadastroId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
