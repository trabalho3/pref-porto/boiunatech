﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestrutura.Migrations
{
    public partial class ArquivosCadastro : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Agência",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaminhoComprovanteBancário",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaminhoComprovanteEndereço",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaminhoDeclaraçãoDeResidência",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CaminhoRGCPF",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Conta",
                table: "Cadastros",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NomeBanco",
                table: "Cadastros",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Agência",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "CaminhoComprovanteBancário",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "CaminhoComprovanteEndereço",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "CaminhoDeclaraçãoDeResidência",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "CaminhoRGCPF",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "Conta",
                table: "Cadastros");

            migrationBuilder.DropColumn(
                name: "NomeBanco",
                table: "Cadastros");
        }
    }
}
