﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores
{
    public class CadastroGerador : ICadastroGerador<CadastroModelo>
    {
        private CadastroModelo _cadastroAtual;
        public ICadastroGerador<CadastroModelo> ComBanco(string nome, string agência, string conta)
        {
            _cadastroAtual.NomeBanco = nome;
            _cadastroAtual.Agência = agência;
            _cadastroAtual.Conta = conta;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComDadosPessoais(string nome, string cpf, DateTime nascimento, string email, string telefone)
        {
            _cadastroAtual.Nome = nome;
            _cadastroAtual.CPF = cpf;
            _cadastroAtual.Nascimento = nascimento;
            _cadastroAtual.Email = email;
            _cadastroAtual.Telefone = telefone;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComDocumentos(string RGCPF, string comprovanteEndereço, string foto, string declaraçãoResidência, string comprovanteBancário)
        {
            _cadastroAtual.CaminhoRGCPF = RGCPF;
            _cadastroAtual.CaminhoComprovanteEndereço = comprovanteEndereço;
            _cadastroAtual.CaminhoFoto = foto;
            _cadastroAtual.CaminhoDeclaraçãoDeResidência = declaraçãoResidência;
            _cadastroAtual.CaminhoComprovanteBancário = comprovanteBancário;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComDocumentosResponsável(string RGCPF, string foto)
        {
            _cadastroAtual.CaminhoRGCPFResponsável = RGCPF;
            _cadastroAtual.CaminhoFotoResponsável = foto;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComEndereço(string endereço, string cep, string cidade)
        {
            _cadastroAtual.Endereço = endereço;
            _cadastroAtual.CEP = cep;
            _cadastroAtual.Cidade = cidade;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComResponsável(string nome, string cpf)
        {
            _cadastroAtual.NomeResponsável = nome;
            _cadastroAtual.CPFResponsável = cpf;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComSenha(string senha, string senhaConfirmação)
        {
            _cadastroAtual.Senha = senha;
            _cadastroAtual.SenhaConfirmação = senha;

            return this;
        }

        public ICadastroGerador<CadastroModelo> ComToken(string token)
        {
            _cadastroAtual.TokenConfirmação = token;
            _cadastroAtual.Confirmado = false;

            return this;
        }

        public ICadastroGerador<CadastroModelo> NovoCadastro()
        {
            _cadastroAtual = new CadastroModelo();
            return this;
        }

        public CadastroModelo ParaCadastro()
        {
            var retorno = _cadastroAtual;
            _cadastroAtual = null;

            return retorno;
        }
    }
}
