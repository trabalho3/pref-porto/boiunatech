﻿using Infraestrutura.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface IDadosValidaçãoGerador
    {
        IDadosValidaçãoViewModel Gerar(bool erro, string mensagem);
        IDadosValidaçãoViewModel GerarErroInesperado();
    }
}
