﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Geradores.Interfaces
{
    public interface ICadastroGerador<T> where T : class, ICadastroModelo
    {

        ICadastroGerador<T> NovoCadastro();
        ICadastroGerador<T> ComDadosPessoais(string nome, string cpf, DateTime nascimento, string email, string telefone);
        ICadastroGerador<T> ComSenha(string senha, string senhaConfirmação);
        ICadastroGerador<T> ComEndereço(string endereço, string cep, string cidade);
        ICadastroGerador<T> ComBanco(string nome, string agência, string conta);
        ICadastroGerador<T> ComResponsável(string nome, string cpf);
        ICadastroGerador<T> ComDocumentos(string RGCPF, string comprovanteEndereço, string foto, string declaraçãoResidência, string comprovanteBancário);
        ICadastroGerador<T> ComDocumentosResponsável(string RGCPF, string foto);
        ICadastroGerador<T> ComToken(string token);
        T ParaCadastro();
    }
}
