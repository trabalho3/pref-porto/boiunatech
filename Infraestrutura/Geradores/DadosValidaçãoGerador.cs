﻿using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using Infraestrutura.ViewModels;
using System;

namespace Infraestrutura.Geradores
{
    public class DadosValidaçãoGerador : IDadosValidaçãoGerador
    {
        public IDadosValidaçãoViewModel Gerar(bool erro, string mensagem) =>  new DadosValidaçãoViewModel { Erro = erro, MensagemErro = mensagem };
        public IDadosValidaçãoViewModel GerarErroInesperado() => new DadosValidaçãoViewModel { Erro = true, MensagemErro = "Algo inesperado ocorreu." };
    }
}
