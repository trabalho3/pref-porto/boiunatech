﻿using Infraestrutura.Singletons.Interfaces;
using System;

namespace Infraestrutura.Singletons
{
    public class AmbienteDebugSingleton : IAmbienteSingleton
    {
        public string BannerSuperior => "/imgs/banner_testes.png";

        public string CaminhoUpload => Environment.GetEnvironmentVariable("DiretorioBoiunaUpload", EnvironmentVariableTarget.User);

        public string PastaRGCPF => "candidato_rgcpf";

        public string PastaComprovanteResidência => "candidato_comprovante_residencia";

        public string PastaDeclaraçãoResidência => "candidato_declaracao_residencia";

        public string PastaComprovanteBancário => "candidato_comprovante_bancario";

        public string PastaRGCPFResponsável => "responsavel_rgcpf";

        public string PastaFoto => "candidato_foto";
        public string PastaFotoResponsável => "responsavel_foto";

        public string ServidorEmail => Environment.GetEnvironmentVariable("ServidorEmail", EnvironmentVariableTarget.User);

        public int ServidorEmailPorta => int.Parse(Environment.GetEnvironmentVariable("ServidorEmailPorta", EnvironmentVariableTarget.User));

        public string UsuárioServidorEmail => Environment.GetEnvironmentVariable("UsuarioServidorEmail", EnvironmentVariableTarget.User);

        public string SenhaServidorEmail => Environment.GetEnvironmentVariable("SenhaServidorEmail", EnvironmentVariableTarget.User);

    }
}
