﻿using Infraestrutura.Singletons.Interfaces;
using System;

namespace Infraestrutura.Singletons
{
    public class AmbienteReleaseSingleton : IAmbienteSingleton
    {
        public string BannerSuperior  => "/imgs/banner_sup.png";

        public string CaminhoUpload => "/uploads";

        public string PastaRGCPF => "candidato_rgcpf";

        public string PastaComprovanteResidência => "candidato_comprovante_residencia";

        public string PastaDeclaraçãoResidência => "candidato_declaracao_residencia";

        public string PastaComprovanteBancário => "candidato_comprovante_bancario";

        public string PastaRGCPFResponsável => "responsavel_rgcpf";

        public string PastaFoto => "candidato_foto";
        public string PastaFotoResponsável => "responsavel_foto";

        public string ServidorEmail => Environment.GetEnvironmentVariable("ServidorEmail", EnvironmentVariableTarget.Process);

        public int ServidorEmailPorta => int.Parse(Environment.GetEnvironmentVariable("ServidorEmailPorta", EnvironmentVariableTarget.Process));

        public string UsuárioServidorEmail => Environment.GetEnvironmentVariable("UsuarioServidorEmail", EnvironmentVariableTarget.Process);

        public string SenhaServidorEmail => Environment.GetEnvironmentVariable("SenhaServidorEmail", EnvironmentVariableTarget.Process);
    }
}
