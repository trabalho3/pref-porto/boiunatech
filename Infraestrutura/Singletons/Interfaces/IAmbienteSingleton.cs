﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestrutura.Singletons.Interfaces
{
    public interface IAmbienteSingleton
    {
        string BannerSuperior { get; }
        string CaminhoUpload { get; }
        string PastaRGCPF { get; }
        public string PastaComprovanteResidência { get; }
        public string PastaDeclaraçãoResidência { get; }
        public string PastaComprovanteBancário { get; }
        public string PastaFoto { get; }

        public string PastaRGCPFResponsável { get; }
        public string PastaFotoResponsável { get; }

        public string ServidorEmail { get; }
        public int ServidorEmailPorta { get; }
        public string UsuárioServidorEmail { get; }
        public string SenhaServidorEmail { get; }
    }
}
