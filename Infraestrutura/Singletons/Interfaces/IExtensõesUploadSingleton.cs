﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Singletons.Interfaces
{
    public interface IExtensõesUploadSingleton
    {
        public string[] ExtensõesPDF { get;}
        public string[] ExtensõesImagem { get;}
    }
}
