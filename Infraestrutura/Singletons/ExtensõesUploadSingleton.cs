﻿using Infraestrutura.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Singletons
{
    public class ExtensõesUploadSingleton : IExtensõesUploadSingleton
    {
        public string[] ExtensõesPDF => new string[] { ".pdf" };
        public string[] ExtensõesImagem => new string[] { ".jpg", ".jpeg", ".png" };
    }
}
