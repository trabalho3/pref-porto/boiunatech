﻿using Infraestrutura.Modelos.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Infraestrutura.Modelos
{
    public class InscriçãoMinecraftModelo : IInscriçãoMinecraftModelo<CadastroModelo>
    {
        [Required]
        public string Nickname { get; set; }

        public bool Aprovada { get; set; }

        public bool Análise { get; set; }

        public CadastroModelo Cadastro { get; set; }

        public bool AceitouTermos { get; set; }

        [Required]
        public int Id { get; set; }
    }
}
