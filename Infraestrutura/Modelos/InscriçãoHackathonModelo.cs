﻿using Infraestrutura.Modelos.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Infraestrutura.Modelos
{
    public class InscriçãoHackathonModelo : IInscriçãoHackathonModelo<CadastroModelo>
    {
        [Required]
        public string NomeEquipe { get; set; }

        public string Membros { get; set; }

        public bool Aprovada { get; set; }

        public bool Análise { get; set; }

        public CadastroModelo Cadastro { get; set; }

        public bool AceitouTermos { get; set; }

        [Required]
        public int Id { get; set; }
    }
}
