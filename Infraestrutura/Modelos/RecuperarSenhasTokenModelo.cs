﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Infraestrutura.Modelos
{
    public class RecuperarSenhasTokenModelo : IRecuperarSenhasTokenModelo<CadastroModelo>
    {
        [Required]
        public string Token { get; set; }
        public bool Usado { get; set; }
        [Required]
        public CadastroModelo Cadastro { get; set; }

        [Required]
        public int Id { get; set; }
    }
}
