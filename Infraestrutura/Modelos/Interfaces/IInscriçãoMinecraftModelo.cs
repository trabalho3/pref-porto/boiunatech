﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface IInscriçãoMinecraftModelo<T> : IInscriçãoModelo<T> where T : ICadastroModelo
    {
        public string Nickname { get;}
    }
}
