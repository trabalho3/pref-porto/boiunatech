﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface IInscriçãoHackathonModelo<T> : IInscriçãoModelo<T> where T: ICadastroModelo
    {
        string NomeEquipe { get; }
        string Membros { get; }
    }
}
