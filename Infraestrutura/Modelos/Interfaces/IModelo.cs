﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface IModelo
    {
        public int Id { get; }
    }
}
