﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface ILoginModelo : IModelo
    {
        public string Nome { get;}
        public string Senha { get; }
        public string Login { get; }
    }
}
