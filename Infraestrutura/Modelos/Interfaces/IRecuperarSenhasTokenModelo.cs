﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface IRecuperarSenhasTokenModelo<T> : IModelo where T: ICadastroModelo
    {
        public string Token { get; set; }
        public bool Usado { get; set; }
        public T Cadastro { get; set; }
    }
}
