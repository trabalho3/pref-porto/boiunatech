﻿using System;

namespace Infraestrutura.Modelos.Interfaces
{
    public interface ICadastroModelo : IModelo
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime Nascimento { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Endereço { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string CaminhoRGCPF { get; set; }
        public string CaminhoComprovanteEndereço { get; set; }
        public string CaminhoFoto { get; set; }
        public string CaminhoDeclaraçãoDeResidência { get; set; }
        public string NomeBanco { get; set; }
        public string Agência { get; set; }
        public string Conta { get; set; }
        public string CaminhoComprovanteBancário { get; set; }
        public string NomeResponsável { get; set; }
        public string CPFResponsável { get; set; }
        public string CaminhoRGCPFResponsável { get; set; }
        public string CaminhoFotoResponsável { get; set; }
        public string SenhaConfirmação { get; set; }
        public string TokenConfirmação { get; set; }
        public bool Confirmado { get; set; }
    }
}
