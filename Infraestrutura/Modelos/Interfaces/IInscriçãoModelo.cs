﻿namespace Infraestrutura.Modelos.Interfaces
{
    public interface IInscriçãoModelo<T>: IModelo where T: ICadastroModelo
    {
        bool Aprovada { get; set; }
        bool Análise { get; set;  }
        T Cadastro { get; set; }
        bool AceitouTermos { get; set; }
    }
}
