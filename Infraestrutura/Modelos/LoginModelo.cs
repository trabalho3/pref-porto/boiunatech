﻿using Infraestrutura.Modelos.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Infraestrutura.Modelos
{
    public class LoginModelo : ILoginModelo
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Senha { get; set; }
        [Required]
        public string Login { get; set; }
    }
}
