﻿using Infraestrutura.Modelos.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infraestrutura.Modelos
{
    public class CadastroModelo : ICadastroModelo
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [Column(TypeName = "char(14)")]
        public string CPF { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public virtual DateTime Nascimento { get; set; }
        [Required]
        [Column(TypeName = "char(15)")]
        public string Telefone { get; set; }
        
        [Required]
        public string Email { get; set; }

        [Required]
        public string Senha { get; set; }

        [Required]
        public string Endereço { get; set; }
        
        [Required]
        public string Cidade { get; set; }

        [Required]
        [Column(TypeName = "char(9)")]
        public string CEP { get; set; }

        [Required]
        public string CaminhoRGCPF { get; set; }

        [Required]
        public string CaminhoComprovanteEndereço { get; set; }
        
        [Required]
        public string CaminhoFoto { get; set; }

        public string CaminhoDeclaraçãoDeResidência { get; set; }

        [Required]
        public string NomeBanco { get; set; }

        [Required]
        public string Agência { get; set; }

        [Required]
        public string Conta { get; set; }

        [Required]
        public string CaminhoComprovanteBancário { get; set; }

        public string NomeResponsável { get; set; }

        [Column(TypeName = "char(14)")]
        public string CPFResponsável { get; set; }

        public string CaminhoRGCPFResponsável { get; set; }
        public string CaminhoFotoResponsável { get; set; }

        [Required]
        public string TokenConfirmação { get; set; }
        public bool Confirmado { get; set; }


        [NotMapped]
        public string SenhaConfirmação { get; set; }
    }
}
