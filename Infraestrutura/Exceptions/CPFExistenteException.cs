﻿using Infraestrutura.Exceptions.Base;
using System;

namespace Infraestrutura.Exceptions
{
    public class CPFExistenteException : ExceptionBase
    {
        private static readonly string _mensagem = "CPF já existe.";
        public CPFExistenteException() : base(_mensagem)
        {
        }

        public CPFExistenteException(Exception innerException) : base(_mensagem, innerException)
        {
        }
    }
}
