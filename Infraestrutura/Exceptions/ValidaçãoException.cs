﻿using Infraestrutura.Exceptions.Base;
using System;

namespace Infraestrutura.Exceptions
{
    public class ValidaçãoException : ExceptionBase
    {
        private static readonly string _prefixoMensagem = "Campo inválido:";
        public ValidaçãoException(string nomeCampo) : base ($"{_prefixoMensagem} {nomeCampo}")
        {
        }

        public ValidaçãoException(string nomeCampo, Exception innerException) : base($"{_prefixoMensagem} {nomeCampo}", innerException)
        {
        }
    }
}
