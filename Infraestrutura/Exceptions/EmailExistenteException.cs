﻿using Infraestrutura.Exceptions.Base;
using System;

namespace Infraestrutura.Exceptions
{
    public class EmailExistenteException : ExceptionBase
    {
        private static readonly string _mensagem = "E-mail já existe";
        public EmailExistenteException() : base(_mensagem)
        {
        }

        public EmailExistenteException(Exception e) : base(_mensagem, e)
        {
        }
    }
}
