﻿using Infraestrutura.Exceptions.Base;
using System;

namespace Infraestrutura.Exceptions
{
    public class ExtensãoInválidaException : ExceptionBase
    {
        private static readonly string _mensagem = "Extensão do arquivo enviado é inválida";
        public ExtensãoInválidaException() : base(_mensagem)
        {
        }

        public ExtensãoInválidaException(Exception innerException) : base(_mensagem, innerException)
        {
        }
    }
}
