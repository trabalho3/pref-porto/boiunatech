﻿using Infraestrutura.Exceptions.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Exceptions
{
    public class ArquivoNuloException : ExceptionBase
    {
        private static readonly string _mensagem = "Arquivo nulo enviado";
        public ArquivoNuloException() : base(_mensagem)
        {
        }

        public ArquivoNuloException(Exception innerException) : base(_mensagem, innerException)
        {
        }
    }
}
