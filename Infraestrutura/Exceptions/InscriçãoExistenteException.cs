﻿using Infraestrutura.Exceptions.Base;
using System;

namespace Infraestrutura.Exceptions
{
    public class InscriçãoExistenteException : ExceptionBase
    {
        private static readonly string _mensagem = "Sua inscrição já foi feita";

        public InscriçãoExistenteException() : base(_mensagem)
        {
        }

        public InscriçãoExistenteException(Exception innerException) : base(_mensagem, innerException)
        {
        }
    }
}
