﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Exceptions.Base
{
    public class ExceptionBase : Exception
    {
        public ExceptionBase(string mensagem) : base(mensagem)
        {
        }

        public ExceptionBase(string mensagem, Exception e) : base(mensagem, e)
        {
        }
    }
}
