﻿using Infraestrutura.Exceptions.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.Exceptions
{
    public class EnviarEmailException : ExceptionBase
    {
        private static readonly string _mensagem = "Erro ao enviar e-mail. Seu cadastro foi realizado e mantido em nossa base de dados. Posteriormente, solicite o reenvio do e-mail no área de login.";

        public EnviarEmailException() : base(_mensagem)
        {
        }

        public EnviarEmailException(Exception e) : base(_mensagem, e)
        {
        }
    }
}
