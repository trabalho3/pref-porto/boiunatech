﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class RedefinirSenhaViewModel : IRedefinirSenhaViewModel
    {
        public string Token { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public IDadosValidaçãoViewModel Validação { get; set; }

        public RedefinirSenhaViewModel()
        {
            Validação = new DadosValidaçãoViewModel(false, "");
        }
    }
}
