﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class HackathonInscriçãoViewModel
    {

        public HackathonInscriçãoViewModel()
        {
            Validação = new DadosValidaçãoViewModel();
        }

        public IDadosValidaçãoViewModel Validação { get; set; }
        public string NomeEquipe { get; set; }
        public string NomeMembros { get; set; }
        public bool Aceitou { get; set; }
    }
}
