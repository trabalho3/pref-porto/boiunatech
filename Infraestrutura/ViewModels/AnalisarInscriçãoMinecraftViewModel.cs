﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class AnalisarInscriçãoMinecraftViewModel<T> where T : ICadastroModelo
    {
        public int Id { get; set; }
        public int CadastroId { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereço { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string Banco { get; set; }
        public string Agência { get; set; }
        public string Conta { get; set; }
        public bool TemDeclaração { get; set; }

        public string NomeResponsável { get; set; }
        public string CPFResponsável { get; set; }
        public bool TemResponsável { get => (!string.IsNullOrEmpty(NomeResponsável) && !(string.IsNullOrEmpty(CPFResponsável))); }

        public string Nickname { get; set; }


        public AnalisarInscriçãoMinecraftViewModel(IInscriçãoMinecraftModelo<T> modelo)
        {
            Id = modelo.Id;
            CadastroId = modelo.Cadastro.Id;
            Nome = modelo.Cadastro.Nome;
            CPF = modelo.Cadastro.CPF;
            Telefone = modelo.Cadastro.Telefone;
            Email = modelo.Cadastro.Email;
            Endereço = modelo.Cadastro.Endereço;
            Cidade = modelo.Cadastro.Cidade;
            CEP = modelo.Cadastro.CEP;
            Banco = modelo.Cadastro.NomeBanco;
            Agência = modelo.Cadastro.Agência;
            Conta = modelo.Cadastro.Conta;
            TemDeclaração = !string.IsNullOrEmpty(modelo.Cadastro.CaminhoDeclaraçãoDeResidência);

            NomeResponsável = modelo.Cadastro.NomeResponsável;
            CPFResponsável = modelo.Cadastro.CPFResponsável;

            Nickname = modelo.Nickname;
        }
    }
}
