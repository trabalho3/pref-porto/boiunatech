﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class AdminInscriçãoMinecraftViewModel<T> where T : class, ICadastroModelo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public string ClasseStatus { get; set; }

        public AdminInscriçãoMinecraftViewModel(IInscriçãoMinecraftModelo<T> modelo)
        {
            Id = modelo.Id;
            Nome = modelo.Cadastro.Nome;
            DeterminarStatus(modelo.Análise, modelo.Aprovada);
        }
        public AdminInscriçãoMinecraftViewModel(IInscriçãoHackathonModelo<T> modelo)
        {
            Id = modelo.Id;
            Nome = modelo.Cadastro.Nome;
            DeterminarStatus(modelo.Análise, modelo.Aprovada);
        }


        internal void DeterminarStatus(bool análise, bool aprovada)
        {
            if(análise)
            {
                Status = "Em Análise";
                ClasseStatus = "badge-info";
            }
            else
            {
                if(aprovada)
                {
                    Status = "Deferida";
                    ClasseStatus = "badge-success";
                }
                else
                {
                    Status = "Indeferida";
                    ClasseStatus = "badge-danger";
                }
            }
        }
    }
}
