﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestrutura.ViewModels.Base
{
    public abstract class BaseAcompanharViewModel
    {
        public string ClasseBadge { get; set; }
        public string Status { get; set; }

        public BaseAcompanharViewModel(bool análise, bool aprovada)
        {
            if (análise)
            {
                Status = "Em Análise";
                ClasseBadge = "badge-info";
            }
            else
            {
                if (aprovada)
                {
                    Status = "Deferida";
                    ClasseBadge = "badge-success";
                }
                else
                {
                    Status = "Indeferida";
                    ClasseBadge = "badge-danger";
                }
            }
        }
    }
}
