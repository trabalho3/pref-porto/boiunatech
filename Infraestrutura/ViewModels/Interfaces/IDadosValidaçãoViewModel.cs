﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface IDadosValidaçãoViewModel
    {
        public bool Erro { get; set; }
        public string MensagemErro { get; set; }
    }
}
