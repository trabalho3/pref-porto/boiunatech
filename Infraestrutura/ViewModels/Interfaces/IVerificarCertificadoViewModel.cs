﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModel.Interfaces
{
    public interface IVerificarCertificadoViewModel
    {
        public string Código { get; }
        public string Nome { get; }
        public bool Verificação { get; }
        public int CargaHorária { get; }
        public DateTime Início { get; }
        public DateTime Fim { get; }
        public string NomeEvento { get; }
    }
}
