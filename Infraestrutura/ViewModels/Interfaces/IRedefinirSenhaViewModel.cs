﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestrutura.ViewModels.Interfaces
{
    public interface IRedefinirSenhaViewModel
    {
        string Token { get; set; }
        string Senha { get; set; }
        string ConfirmarSenha { get; set; }
        IDadosValidaçãoViewModel Validação { get; set; }
    }
}
