﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class AdminViewModel
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }

        public IDadosValidaçãoViewModel Validação { get; set; }

        public AdminViewModel()
        {
            Validação = new DadosValidaçãoViewModel();
        }
    }
}
