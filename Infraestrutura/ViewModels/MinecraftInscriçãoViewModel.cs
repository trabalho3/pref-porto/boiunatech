﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class MinecraftInscriçãoViewModel
    {
        public IDadosValidaçãoViewModel Validação { get; set; }
        public string Nickname { get; set; }
        public bool Aceitou { get; set; }

        public MinecraftInscriçãoViewModel()
        {
            Validação = new DadosValidaçãoViewModel();
        }
    }
}
