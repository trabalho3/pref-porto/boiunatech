﻿using Infraestrutura.Modelos.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class AnalisarInscriçãoHackathonViewModel<T> where T: ICadastroModelo
    {
        public int Id { get; set; }
        public int CadastroId { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereço { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string Banco { get; set; }
        public string Agência { get; set; }
        public string Conta { get; set; }
        public bool TemDeclaração { get; set; }
        public string Foto { get; set; }

        public string NomeResponsável { get; set; }
        public string CPFResponsável { get; set; }
        public bool TemResponsável { get => (!string.IsNullOrEmpty(NomeResponsável) && !(string.IsNullOrEmpty(CPFResponsável))); }

        public string NomeDaEquipe { get; set; }
        public string Membros { get; set; }


        public AnalisarInscriçãoHackathonViewModel(IInscriçãoHackathonModelo<T> modelo)
        {
            Id = modelo.Id;
            CadastroId = modelo.Cadastro.Id;
            Nome = modelo.Cadastro.Nome;
            CPF = modelo.Cadastro.CPF;
            Foto = modelo.Cadastro.CaminhoFoto;
            Telefone = modelo.Cadastro.Telefone;
            Email = modelo.Cadastro.Email;
            Endereço = modelo.Cadastro.Endereço;
            Cidade = modelo.Cadastro.Cidade;
            CEP = modelo.Cadastro.CEP;
            Banco = modelo.Cadastro.NomeBanco;
            Agência = modelo.Cadastro.Agência;
            Conta = modelo.Cadastro.Conta;
            TemDeclaração = !string.IsNullOrEmpty(modelo.Cadastro.CaminhoDeclaraçãoDeResidência);

            NomeResponsável = modelo.Cadastro.NomeResponsável;
            CPFResponsável = modelo.Cadastro.CPFResponsável;

            NomeDaEquipe = modelo.NomeEquipe;
            Membros = modelo.Membros;
        }
    }
}
