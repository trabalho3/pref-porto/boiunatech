﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class DadosValidaçãoViewModel : IDadosValidaçãoViewModel
    {
        public DadosValidaçãoViewModel(bool erro, string mensagemErro)
        {
            Erro = erro;
            MensagemErro = mensagemErro;
        }

        public DadosValidaçãoViewModel()
        {

        }

        public bool Erro { get; set; }
        public string MensagemErro { get; set; }
    }
}
