﻿using Infraestrutura.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Infraestrutura.ViewModels
{
    public class VerificarCertificadoViewModel : IVerificarCertificadoViewModel
    {
        public string Código { get; set; }

        [JsonPropertyName("nome")]
        public string Nome { get; set; }

        [JsonPropertyName("verificação")]
        public bool Verificação { get; set; }

        [JsonPropertyName("cargaHorária")]
        public int CargaHorária { get; set; }

        [JsonPropertyName("início")]
        public DateTime Início { get; set; }

        [JsonPropertyName("fim")]
        public DateTime Fim { get; set; }

        [JsonPropertyName("nomeEvento")]
        public string NomeEvento { get; set; }
    }
}
