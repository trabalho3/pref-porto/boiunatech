﻿using Infraestrutura.ViewModels.Interfaces;
using Microsoft.AspNetCore.Http;
using System;

namespace Infraestrutura.ViewModels
{
    public class CadastroViewModel
    {
        public CadastroViewModel()
        {
            Validação = new DadosValidaçãoViewModel();
        }

        public IDadosValidaçãoViewModel Validação { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime Nascimento { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Senha { get; set; }
        public string SenhaConfirmação { get; set; }
        public string Endereço { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public IFormFile RGCPF { get; set; }
        public IFormFile ComprovanteResidência { get; set; }
        public IFormFile DeclaraçãoResidência { get; set; }
        public IFormFile Foto { get; set; }

        public string NomeBanco { get; set; }
        public string Agência { get; set; }
        public string Conta { get; set; }
        public IFormFile ComprovanteBancário { get; set; }

        public string NomeResponsável { get; set; }
        public string CPFResponsável { get; set; }
        public IFormFile RGCPFResponsável { get; set; }
        public IFormFile FotoResponsável { get; set; }
    }
}
