﻿using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels.Base;

namespace Infraestrutura.ViewModels
{
    public class AcompanharMinecraftViewModel<T> : BaseAcompanharViewModel where T: ICadastroModelo
    {

        public string Nickname { get; set; }

        public AcompanharMinecraftViewModel(IInscriçãoMinecraftModelo<T> modelo) : base(modelo.Análise, modelo.Aprovada)
        {
            Nickname = modelo.Nickname;
        }
    }
}
