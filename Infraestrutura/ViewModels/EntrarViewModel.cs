﻿using Infraestrutura.ViewModels.Interfaces;

namespace Infraestrutura.ViewModels
{
    public class EntrarViewModel
    {

        public EntrarViewModel()
        {
            Validação = new DadosValidaçãoViewModel();
        }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public string Id { get; set; }
        public IDadosValidaçãoViewModel Validação { get; set; }


    }
}
