﻿using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.ViewModels.Base;

namespace Infraestrutura.ViewModels
{
    public class AcompanharHackathonViewModel<T> : BaseAcompanharViewModel where T: ICadastroModelo
    {
        public string NomeEquipe { get; set; }
        public string Membros { get; set; }
        public AcompanharHackathonViewModel(IInscriçãoHackathonModelo<T> modelo) : base(modelo.Análise, modelo.Aprovada)
        {
            NomeEquipe = modelo.NomeEquipe;
            if(string.IsNullOrEmpty(modelo.Membros) || string.IsNullOrWhiteSpace(modelo.Membros))
            {
                Membros = "Nenhum membro.";
            }
            else
            {
                Membros = modelo.Membros;
            }
        }
    }
}
