using Infraestrutura;
using Infraestrutura.Modelos;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.Cookies;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Serviços;
using Infraestrutura.Singletons.Interfaces;
using Infraestrutura.Singletons;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Geradores;
using System.Threading.Tasks;

namespace BoiunaSite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<Contexto>();

#if DEBUG || RELEASE

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                  .AddCookie(options =>
                  {
                      options.LoginPath = "/Competidor/Logar";
                      options.AccessDeniedPath = "/Identity/Negado";
                  });
#else
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Events = new CookieAuthenticationEvents()
                    {
                        OnRedirectToLogin = (context) =>
                        {
                            context.HttpContext.Response.Redirect("https://boiuna.portonacional.to.gov.br:8082/Competidor/Logar");
                            return Task.CompletedTask;
                        },

                        OnRedirectToAccessDenied = (context) =>
                        {
                            context.HttpContext.Response.Redirect("https://boiuna.portonacional.to.gov.br:8082/Identity/Negado");
                            return Task.CompletedTask;
                        }
                    };
                });
#endif
            services.AddScoped<ICriptografiaServiço, CriptografiaServiço>();
            services.AddScoped<IUploadServiço, UploadServiço>();
            services.AddScoped<IEmailServiço, EmailServiço>();
            services.AddScoped<IDadosValidaçãoGerador, DadosValidaçãoGerador>();
            services.AddScoped<ICadastroGerador<CadastroModelo>, CadastroGerador>();
            services.AddScoped<IValidadorGeralServiço, ValidadorGeralServiço>();
            services.AddScoped<IValidadorCadastroServiço, ValidadorCadastroServiço>();
            services.AddScoped<IValidadorFazerLoginServiço, ValidadorFazerLoginServiço>();
            services.AddScoped<IValidadorInscriçãoMinecraftServiço, ValidadorInscriçãoMinecraftServiço>();
            services.AddScoped<IValidadorInscriçãoHackathonServiço, ValidadorInscriçãoHackathonServiço>();
            services.AddScoped<IValidadorRedefinirSenhaViewModelServiço, ValidadorRedefinirSenhaViewModelServiço>();

            services.AddSingleton<IExtensõesUploadSingleton, ExtensõesUploadSingleton>();

#if DEBUG
            services.AddSingleton<IAmbienteSingleton, AmbienteDebugSingleton>();
#elif DOCKERDEBUG
            services.AddSingleton<IAmbienteSingleton, AmbienteDockerDebugSingleton>();
#else
            services.AddSingleton<IAmbienteSingleton, AmbienteReleaseSingleton>();
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
