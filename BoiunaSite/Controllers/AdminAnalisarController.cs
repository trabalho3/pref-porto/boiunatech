﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class AdminAnalisarController : Controller
    {
        private readonly Contexto _contexto;
        private readonly DbSet<InscriçãoMinecraftModelo> _dadosMinecraft;
        private readonly DbSet<InscriçãoHackathonModelo> _dadosHackathon;

        public AdminAnalisarController(Contexto contexto)
        {
            _contexto = contexto;
            _dadosMinecraft = _contexto.Set<InscriçãoMinecraftModelo>();
            _dadosHackathon = _contexto.Set<InscriçãoHackathonModelo>();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Minecraft()
        {
            var inscrições = _dadosMinecraft.OrderBy(m => m.Id)
                .Include(m => m.Cadastro)
                .Select(m => new AdminInscriçãoMinecraftViewModel<CadastroModelo>(m))
                .ToList();

            return View("ListarMine", inscrições);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Hackathon()
        {
            var inscrições = _dadosHackathon.OrderBy(h => h.Id)
                .Include(h => h.Cadastro)
                .Select(h => new AdminInscriçãoMinecraftViewModel<CadastroModelo>(h))
                .ToList();

            return View("ListarHackathon", inscrições);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult AnalisarMinecraft(int id)
        {
            var modelo = _dadosMinecraft.Where(m => m.Id == id)
                .Include(m => m.Cadastro)
                .SingleOrDefault();

            var viewModel = new AnalisarInscriçãoMinecraftViewModel<CadastroModelo>(modelo);
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult AnalisarHackathon(int id)
        {
            var modelo = _dadosHackathon.Where(h => h.Id == id)
                .Include(h => h.Cadastro)
                .SingleOrDefault();

            var viewModel = new AnalisarInscriçãoHackathonViewModel<CadastroModelo>(modelo);
            return View(viewModel);
        }
    }
}
