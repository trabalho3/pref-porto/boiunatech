﻿using System;
using System.Linq;
using Infraestrutura;
using Infraestrutura.Exceptions.Base;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class EsqueciSenhaController : Controller
    {
        private readonly Contexto _contexto;
        private readonly IEmailServiço _email;
        private readonly IValidadorRedefinirSenhaViewModelServiço _validadorRedefinirSenha;
        private readonly IDadosValidaçãoGerador _geradorDadosValiudação;
        private readonly ICriptografiaServiço _criptografiaServiço;
        private readonly DbSet<CadastroModelo> _dadosCadastro;
        private readonly DbSet<RecuperarSenhasTokenModelo> _dadosRecuperarSenha;

        public EsqueciSenhaController(Contexto contexto, IEmailServiço email, IValidadorRedefinirSenhaViewModelServiço validadorRedefinirSenha, IDadosValidaçãoGerador geradorDadosValiudação,
            ICriptografiaServiço criptografiaServiço)
        {
            _contexto = contexto;
            _email = email;
            _validadorRedefinirSenha = validadorRedefinirSenha;
            _geradorDadosValiudação = geradorDadosValiudação;
            _criptografiaServiço = criptografiaServiço;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
            _dadosRecuperarSenha = _contexto.Set<RecuperarSenhasTokenModelo>();
        }

        public IActionResult Index()
        {
            return View(new ReenviarEmailViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Lembrar(ReenviarEmailViewModel modelo)
        {
            var correspondência = _dadosCadastro.Where(c => c.Email == modelo.Email).SingleOrDefault();
            if(correspondência != null)
            {
                var token = _email.GerarToken();
                var recuperarSenha = new RecuperarSenhasTokenModelo() { Cadastro = correspondência, Token = token };
                _dadosRecuperarSenha.Add(recuperarSenha);
                _contexto.SaveChanges();
                _email.EnviarRecuperarSenha(token, modelo.Email);
            }

            return View("Sucesso", modelo);
        }

        [HttpGet("{controller}/{action}/{token}")]
        public IActionResult Redefinir(string token)
        {
            var modelo = new RedefinirSenhaViewModel();
            var correspondência = _dadosRecuperarSenha.Where(r => r.Token == token && !r.Usado).SingleOrDefault();
            if(correspondência == null)
            {
                return View("NadaEncontrado");
            }

            modelo.Token = token;
            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DefinirSenha(RedefinirSenhaViewModel modelo)
        {
            try
            {
                _validadorRedefinirSenha.ValidarDadosInseridos(modelo); //Senhas iguais e não vazias

                var correspondência = _dadosRecuperarSenha.Where(r => r.Token == modelo.Token && !r.Usado)
                    .Include(r => r.Cadastro)
                    .SingleOrDefault();

                if (correspondência == null)
                {
                    return View("NadaEncontrado");
                }

                correspondência.Usado = true;
                correspondência.Cadastro.Senha = _criptografiaServiço.Criptografar(modelo.Senha);

                _dadosCadastro.Update(correspondência.Cadastro);
                _dadosRecuperarSenha.Update(correspondência);
                _contexto.SaveChanges();

                return View("SucessoRedefinir");

            }
            catch(Exception e)
            {
                if(e is ExceptionBase)
                {
                    modelo.Validação = _geradorDadosValiudação.Gerar(true, e.Message);
                }
                else
                {
                    modelo.Validação = _geradorDadosValiudação.GerarErroInesperado();
                }

                modelo.Senha = "";
                modelo.ConfirmarSenha = "";
                return View(nameof(Redefinir), modelo);

            }
        }
    }
}
