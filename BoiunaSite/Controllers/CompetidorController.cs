﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infraestrutura;
using Infraestrutura.Exceptions.Base;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class CompetidorController : Controller
    {
        private readonly Contexto _contexto;
        private readonly IValidadorFazerLoginServiço _validadorFazerLogin;
        private readonly IDadosValidaçãoGerador _geradorDadosValidação;
        private readonly ICriptografiaServiço _criptografiaServiço;
        private readonly DbSet<CadastroModelo> _dadosCadastro;
        private readonly DbSet<InscriçãoMinecraftModelo> _dadosMinecraft;
        private readonly DbSet<InscriçãoHackathonModelo> _dadosHackathon;

        public CompetidorController(Contexto contexto, IValidadorFazerLoginServiço validadorFazerLogin, IDadosValidaçãoGerador geradorDadosValidação, ICriptografiaServiço criptografiaServiço)
        {
            _contexto = contexto;
            _validadorFazerLogin = validadorFazerLogin;
            _geradorDadosValidação = geradorDadosValidação;
            _criptografiaServiço = criptografiaServiço;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
            _dadosMinecraft = _contexto.Set<InscriçãoMinecraftModelo>();
            _dadosHackathon = _contexto.Set<InscriçãoHackathonModelo>();
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Index()
        {
            var claim = User.Claims.Where(c => c.Type == "Id").Single();
            var id = int.Parse(claim.Value);

            var temInscriçãoMinecraft = _dadosMinecraft.Where(m => m.Cadastro.Id == id).SingleOrDefault() != null;
            var temInscriçãoHackathon = _dadosHackathon.Where(h => h.Cadastro.Id == id).SingleOrDefault() != null;

            var viewModel = new PainelCompetidorViewModel
            {
                TemMinecraft = temInscriçãoMinecraft,
                TemHackathon = temInscriçãoHackathon
            };
            return View(viewModel);
        }

        public IActionResult Logar()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Competidor")) return RedirectToAction(nameof(Index)); //Usuário já logado não precisa logar de novo

            return View(new EntrarViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult FazerLogin(EntrarViewModel modelo)
        {
            try
            {
                _validadorFazerLogin.ValidarDadosInseridos(modelo.Email, modelo.Senha);
                var senhaCriptografada = _criptografiaServiço.Criptografar(modelo.Senha);
                var correspondência = _dadosCadastro.Where(c => c.Email == modelo.Email && c.Senha == senhaCriptografada).SingleOrDefault();
                _validadorFazerLogin.ValidarCorrespondência(correspondência); //Se for nulo (nada encontrado) se não for confirmado (e-maikl não confirmado)

                modelo.Senha = "";
                modelo.Nome = correspondência.Nome;
                modelo.Id = correspondência.Id.ToString();
                Login(modelo);

                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                if(e is ExceptionBase)
                {
                    modelo.Validação = _geradorDadosValidação.Gerar(true, e.Message);
                }

                else
                {
                    modelo.Validação = _geradorDadosValidação.GerarErroInesperado();
                }

                return View(nameof(Logar), modelo);
            }
        }

        [Authorize(Roles = "Competidor")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Logar));
        }


        private async void Login(EntrarViewModel modelo)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, modelo.Email),
                new Claim("Id", modelo.Id),
                new Claim(ClaimTypes.Name, modelo.Nome),
                new Claim(ClaimTypes.Role, "Competidor")
            };

            var identidadeDeUsuário = new ClaimsIdentity(claims, "Login");
            var claimsPrincipal = new ClaimsPrincipal(identidadeDeUsuário);
            var propriedades = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTime.Now.ToLocalTime().AddHours(2),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, propriedades);
        }
    }
}
