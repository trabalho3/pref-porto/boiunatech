﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Infraestrutura.Modelos.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class ArquivosUploadsController : Controller
    {

        private readonly Contexto _contexto;
        private readonly DbSet<CadastroModelo> _dadosCadastro;

        public ArquivosUploadsController(Contexto contexto)
        {
            _contexto = contexto;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult MostrarRGCPF(int id)
        {
            var caminho = ObterCaminhoRGCPF(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, "application/pdf");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult MostrarComprovante(int id)
        {
            var caminho = ObterCaminhoComprovante(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, "application/pdf");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult MostrarComprovanteBanco(int id)
        {
            var caminho = ObterCaminhoComprovanteBancário(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, "application/pdf");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult MostrarDeclaraçãoResidência(int id)
        {
            var caminho = ObterCaminhoDeclaraçãoResidência(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, "application/pdf");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult MostrarRGCPFResponsavel(int id)
        {
            var caminho = ObterCaminhoRFCPFResponsável(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, "application/pdf");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult MostrarSelfieResponsavel(int id)
        {
            var caminho = ObterCaminhoSelfieResponsável(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, $"image/{Extensão(caminho)}");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult MostrarSelfie(int id)
        {
            var caminho = ObterCaminhoSelfie(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, $"image/{Extensão(caminho)}");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult MostrarFoto(int id)
        {
            var caminho = ObterCaminhoFoto(id);
            var bytes = RetornarArquivo(caminho);
            return File(bytes, $"image/{Extensão(caminho)}");
        }

        private string ObterCaminhoFoto(int id) => ObterCadastro(id).CaminhoFoto;
        private string ObterCaminhoSelfie(int id) => ObterCadastro(id).CaminhoFoto;
        private string Extensão(string caminho) => caminho.Split(".").Last();
        private string ObterCaminhoSelfieResponsável(int id) => ObterCadastro(id).CaminhoFotoResponsável;
        private string ObterCaminhoRFCPFResponsável(int id) => ObterCadastro(id).CaminhoRGCPFResponsável;
        private byte[] RetornarArquivo(string caminho) => System.IO.File.ReadAllBytes(caminho);
        private string ObterCaminhoRGCPF(int id) => ObterCadastro(id).CaminhoRGCPF;
        private string ObterCaminhoComprovante(int id) => ObterCadastro(id).CaminhoComprovanteEndereço;
        private string ObterCaminhoComprovanteBancário(int id) => ObterCadastro(id).CaminhoComprovanteBancário;
        private string ObterCaminhoDeclaraçãoResidência(int id) => ObterCadastro(id).CaminhoDeclaraçãoDeResidência;
        private ICadastroModelo ObterCadastro(int id) => _dadosCadastro.Where(c => c.Id == id).SingleOrDefault();
    }
}
