﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Infraestrutura.ViewModel;
using Infraestrutura.ViewModel.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BoiunaSite.Controllers
{
    public class CertificadosController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[controller]/[action]/{código}")]
        public async Task<IActionResult> Verificar(string código)
        {
            var resultado = await RetornarVerificação(código);
            return View(resultado);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerificarFormulário(VerificarCertificadoViewModel dados)
        {
            var resultado = await RetornarVerificação(dados.Código);
            return View("Verificar", resultado);
        }

        private async Task<IVerificarCertificadoViewModel> RetornarVerificação(string Código)
        {
            var url = $"https://boiuna.portonacional.to.gov.br:5200/Certificados/Verificar/{Código}";
            using var cliente = new HttpClient();
            var resposta = await cliente.GetAsync(url);

            resposta.EnsureSuccessStatusCode();

            var retorno = JsonSerializer.Deserialize<VerificarCertificadoViewModel>(await resposta.Content.ReadAsStringAsync());

            retorno.Código = Código;

            return retorno;
        }
    }
}
