﻿using System;
using Infraestrutura;
using Infraestrutura.Exceptions;
using Infraestrutura.Exceptions.Base;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.Singletons.Interfaces;
using Infraestrutura.ViewModels.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BoiunaSite.Controllers
{
    public class CadastroController : Controller
    {
        private readonly Contexto _contexto;
        private readonly ICadastroGerador<CadastroModelo> _geradorCadastro;
        private readonly IDadosValidaçãoGerador _geradorDadosValidação;
        private readonly IUploadServiço _uploader;
        private readonly IAmbienteSingleton _ambiente;
        private readonly IEmailServiço _email;
        private readonly IExtensõesUploadSingleton _extensões;
        private readonly IValidadorCadastroServiço _validadorCadastro;
        private readonly ICriptografiaServiço _criptografia;
        private readonly DbSet<CadastroModelo> _dadosCadastro;

        public CadastroController(Contexto contexto, ICadastroGerador<CadastroModelo> geradorCadastro, IDadosValidaçãoGerador geradorDadosValidação, 
            IUploadServiço uploader, IAmbienteSingleton ambiente, IEmailServiço email, 
            IExtensõesUploadSingleton extensões, IValidadorCadastroServiço validadorCadastro, ICriptografiaServiço criptografia)
        {
            _contexto = contexto;
            _geradorCadastro = geradorCadastro;
            _geradorDadosValidação = geradorDadosValidação;
            _uploader = uploader;
            _ambiente = ambiente;
            _email = email;
            _extensões = extensões;
            _validadorCadastro = validadorCadastro;
            _criptografia = criptografia;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
        }

        public IActionResult Index()
        {
            return View("Proibido");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Cadastrar(CadastroViewModel viewModel)
        {
            string uploadRGCPF = null;
            string uploadComprovanteEndereço = null;
            string uploadComprovanteBancário = null;
            string uploadFoto = null;
            string uploadDeclaraçãoResidência = null;
            string uploadRGCPFResponsável = null;
            string uploadFotoResponsável = null;

            try
            {
                uploadRGCPF = _uploader.FazerUpload(viewModel.RGCPF, _ambiente.PastaRGCPF, _extensões.ExtensõesPDF);
                uploadComprovanteEndereço = _uploader.FazerUpload(viewModel.ComprovanteResidência, _ambiente.PastaComprovanteResidência, _extensões.ExtensõesPDF);
                uploadComprovanteBancário = _uploader.FazerUpload(viewModel.ComprovanteBancário, _ambiente.PastaComprovanteBancário, _extensões.ExtensõesPDF);
                uploadFoto = _uploader.FazerUpload(viewModel.Foto, _ambiente.PastaFoto, _extensões.ExtensõesImagem);

                //Arquivo não obrigatório pode ser nulo, logo, só será feito o upload se o usuário tiver enviado algo
                if (!_uploader.ArquivoNulo(viewModel.DeclaraçãoResidência)) uploadDeclaraçãoResidência = _uploader.FazerUpload(viewModel.DeclaraçãoResidência, _ambiente.PastaDeclaraçãoResidência, _extensões.ExtensõesPDF);
                if (!_uploader.ArquivoNulo(viewModel.RGCPFResponsável)) uploadRGCPFResponsável = _uploader.FazerUpload(viewModel.RGCPFResponsável, _ambiente.PastaRGCPFResponsável, _extensões.ExtensõesPDF);
                if (!_uploader.ArquivoNulo(viewModel.FotoResponsável)) uploadFotoResponsável = _uploader.FazerUpload(viewModel.FotoResponsável, _ambiente.PastaFotoResponsável, _extensões.ExtensõesImagem);

                var token = _email.GerarToken();

                var cadastro = _geradorCadastro.NovoCadastro()
                    .ComDadosPessoais(viewModel.Nome, viewModel.CPF, viewModel.Nascimento, viewModel.Email, viewModel.Telefone)
                    .ComSenha(viewModel.Senha, viewModel.SenhaConfirmação)
                    .ComEndereço(viewModel.Endereço, viewModel.CEP, viewModel.Cidade)
                    .ComBanco(viewModel.NomeBanco, viewModel.Agência, viewModel.Conta)
                    .ComResponsável(viewModel.NomeResponsável, viewModel.CPFResponsável)
                    .ComDocumentos(uploadRGCPF, uploadComprovanteEndereço, uploadFoto, uploadDeclaraçãoResidência, uploadComprovanteBancário)
                    .ComDocumentosResponsável(uploadRGCPFResponsável, uploadFotoResponsável)
                    .ComToken(token)
                    .ParaCadastro();

                _validadorCadastro.Validar(cadastro);

                foreach (var item in _dadosCadastro)
                {
                    if (item.CPF == viewModel.CPF) throw new CPFExistenteException();
                    if (item.Email == viewModel.Email) throw new EmailExistenteException();
                }

                cadastro.Senha = _criptografia.Criptografar(cadastro.Senha);
                _dadosCadastro.Add(cadastro);
                _contexto.SaveChanges();

                try
                {
                    _email.EmailConfirmação(token, viewModel.Email);
                }
                catch
                {
                    throw new EnviarEmailException();
                }

                cadastro.Senha = "";
                cadastro.SenhaConfirmação = "";

                return View("Sucesso", cadastro);
            }
            catch (Exception e)
            {
                _uploader.DeletarUpload(uploadRGCPF);
                _uploader.DeletarUpload(uploadComprovanteBancário);
                _uploader.DeletarUpload(uploadComprovanteEndereço);
                _uploader.DeletarUpload(uploadDeclaraçãoResidência);
                _uploader.DeletarUpload(uploadRGCPFResponsável);
                _uploader.DeletarUpload(uploadFoto);
                _uploader.DeletarUpload(uploadFotoResponsável);

                IDadosValidaçãoViewModel validação;

                if (e is ExceptionBase)
                {
                    validação = _geradorDadosValidação.Gerar(true, e.Message);

                }
                else
                {
                    validação = _geradorDadosValidação.GerarErroInesperado();
                }

                viewModel.Validação = validação;
                return View("Index", viewModel);
            }
        }
    }
}
