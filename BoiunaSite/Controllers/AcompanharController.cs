﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class AcompanharController : Controller
    {
        private readonly Contexto _contexto;
        private readonly DbSet<InscriçãoHackathonModelo> _dadosHackathon;
        private readonly DbSet<InscriçãoMinecraftModelo> _dadosMinecraft;

        public AcompanharController(Contexto contexto)
        {
            _contexto = contexto;
            _dadosHackathon = _contexto.Set<InscriçãoHackathonModelo>();
            _dadosMinecraft = _contexto.Set<InscriçãoMinecraftModelo>();
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Minecraft()
        {
            var id = RetornarId();
            var inscrição = _dadosMinecraft.Where(m => m.Cadastro.Id == id).SingleOrDefault();
            if(inscrição == null) return View("NadaEncontrado");


            var viewModel = new AcompanharMinecraftViewModel<CadastroModelo>(inscrição);
            return View(viewModel);
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Hackathon()
        {
            var id = RetornarId();
            var inscrição = _dadosHackathon.Where(h => h.Cadastro.Id == id).SingleOrDefault();
            if (inscrição == null) return View("NadaEncontrado");

            var viewModel = new AcompanharHackathonViewModel<CadastroModelo>(inscrição);
            return View(viewModel);

        }

        private int RetornarId()
        {
            return int.Parse(User.Claims.Where(c => c.Type == "Id").Single().Value);
        }
    }
}
