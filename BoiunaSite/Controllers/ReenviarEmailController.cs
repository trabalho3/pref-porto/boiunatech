﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class ReenviarEmailController : Controller
    {
        private readonly Contexto _contexto;
        private readonly IEmailServiço _email;
        private readonly DbSet<CadastroModelo> _dadosCadastro;

        public ReenviarEmailController(Contexto contexto, IEmailServiço email)
        {
            _contexto = contexto;
            _email = email;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
        }

        public IActionResult Index()
        {
            return View(new ReenviarEmailViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Reenviar(ReenviarEmailViewModel modelo)
        {
            var correspondência = _dadosCadastro.Where(c => c.Email == modelo.Email).SingleOrDefault();
            if(correspondência != null && !correspondência.Confirmado)
            {
                _email.ReenviarEmail(correspondência.TokenConfirmação, correspondência.Email);
            }

            return View("Sucesso", modelo);
        }
    }
}
