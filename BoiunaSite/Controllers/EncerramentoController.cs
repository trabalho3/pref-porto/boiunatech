﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BoiunaSite.Controllers
{
    public class EncerramentoController : Controller
    {
        public IActionResult Minecraft()
        {
            return View();
        }

        public IActionResult Hackathon()
        {
            return View();
        }

        public IActionResult PódioHackathon()
        {
            return View();
        }

        public IActionResult PódioMinecraft()
        {
            return View();
        }
    }
}
