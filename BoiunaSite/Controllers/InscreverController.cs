﻿using System;
using System.Linq;
using Infraestrutura;
using Infraestrutura.Exceptions.Base;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class InscreverController : Controller
    {
        private readonly Contexto _contexto;
        private readonly IDadosValidaçãoGerador _geradorDadosValidação;
        private readonly IValidadorInscriçãoHackathonServiço _validadorHackathon;
        private readonly IValidadorInscriçãoMinecraftServiço _validadorMinecraft;
        private readonly DbSet<CadastroModelo> _dadosCadastro;
        private readonly DbSet<InscriçãoMinecraftModelo> _dadosMinecraft;
        private readonly DbSet<InscriçãoHackathonModelo> _dadosHackathon;

        public InscreverController(Contexto contexto, IDadosValidaçãoGerador geradorDadosValidação, IValidadorInscriçãoHackathonServiço validadorHackathon, IValidadorInscriçãoMinecraftServiço validadorMinecraft)
        {
            _contexto = contexto;
            _geradorDadosValidação = geradorDadosValidação;
            _validadorHackathon = validadorHackathon;
            _validadorMinecraft = validadorMinecraft;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
            _dadosMinecraft = _contexto.Set<InscriçãoMinecraftModelo>();
            _dadosHackathon = _contexto.Set<InscriçãoHackathonModelo>();
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Minecraft()
        {
            return View(new MinecraftInscriçãoViewModel());
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Hackathon()
        {
            return View(new HackathonInscriçãoViewModel());
        }

        [Authorize(Roles = "Competidor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InscreverMinecraft(MinecraftInscriçãoViewModel modelo)
        {
            try
            {
                var cadastro = ObterCompetidor();

                var inscrição = new InscriçãoMinecraftModelo
                {
                    AceitouTermos = modelo.Aceitou,
                    Nickname = modelo.Nickname,
                    Cadastro = cadastro,
                    Análise = true
                };
                _validadorMinecraft.Validar(inscrição);
                _dadosMinecraft.Add(inscrição);
                _contexto.SaveChanges();

                return RedirectToAction(nameof(Sucesso));
            }
            catch (Exception e)
            {
                if (e is ExceptionBase)
                {
                    modelo.Validação = _geradorDadosValidação.Gerar(true, e.Message);

                }
                else
                {
                    modelo.Validação = _geradorDadosValidação.GerarErroInesperado();

                }
                return View(nameof(Minecraft), modelo);
            }
        }



        [Authorize(Roles = "Competidor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult InscreverHackathon(HackathonInscriçãoViewModel modelo)
        {
            try
            {
                var cadastro = ObterCompetidor();

                var inscrição = new InscriçãoHackathonModelo
                {
                    AceitouTermos = modelo.Aceitou,
                    NomeEquipe = modelo.NomeEquipe,
                    Membros = modelo.NomeMembros,
                    Cadastro = cadastro,
                    Análise = true
                };

                _validadorHackathon.Validar(inscrição);
                _dadosHackathon.Add(inscrição);
                _contexto.SaveChanges();

                return RedirectToAction(nameof(Sucesso));
            }
            catch (Exception e)
            {
                if (e is ExceptionBase)
                {
                    modelo.Validação = _geradorDadosValidação.Gerar(true, e.Message);

                }
                else
                {
                    modelo.Validação = _geradorDadosValidação.GerarErroInesperado();

                }
                return View(nameof(Hackathon), modelo);
            }
        }

        [Authorize(Roles = "Competidor")]
        public IActionResult Sucesso()
        {
            return View();
        }

        private CadastroModelo ObterCadastro(int id) => _dadosCadastro.Where(c => c.Id == id).Single();
        private CadastroModelo ObterCompetidor()
        {
            var claim = User.Claims.Where(c => c.Type == "Id").Single();
            var id = int.Parse(claim.Value);
            var cadastro = ObterCadastro(id);
            return cadastro;
        }
    }
}
