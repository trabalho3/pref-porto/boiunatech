﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class ConfirmarEmailController : Controller
    {

        private readonly Contexto _contexto;
        private readonly DbSet<CadastroModelo> _dadosCadastro;

        public ConfirmarEmailController(Contexto contexto)
        {
            _contexto = contexto;
            _dadosCadastro = _contexto.Set<CadastroModelo>();
        }

        [HttpGet("{controller}/{action}/{token}")]
        public IActionResult Token(string token)
        {
            var inscrição = _dadosCadastro.Where(c => c.TokenConfirmação == token && !c.Confirmado).SingleOrDefault();
            if(inscrição == null)
            {
                return View("NadaEncontrado");
            }

            inscrição.Confirmado = true;
            _dadosCadastro.Update(inscrição);
            _contexto.SaveChanges();

            return View("Sucesso");
        }
    }
}
