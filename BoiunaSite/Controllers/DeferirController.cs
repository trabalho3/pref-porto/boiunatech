﻿using System.Linq;
using Infraestrutura;
using Infraestrutura.Modelos;
using Infraestrutura.Modelos.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class DeferirController : Controller
    {

        private readonly Contexto _contexto;
        private readonly DbSet<InscriçãoMinecraftModelo> _dadosMinecraft;
        private readonly DbSet<InscriçãoHackathonModelo> _dadosHackathon;

        public DeferirController(Contexto contexto)
        {
            _contexto = contexto;
            _dadosMinecraft = _contexto.Set<InscriçãoMinecraftModelo>();
            _dadosHackathon = _contexto.Set<InscriçãoHackathonModelo>();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult DeferirMinecraft(int id)
        {
            var inscrição = MinecraftPorId(id);
            Deferir(inscrição);
            AtualizarMinecraft(inscrição);

            return View("Sucesso");
        }

 

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult IndeferirMinecraft(int id)
        {
            var inscrição = MinecraftPorId(id);
            Indeferir(inscrição);
            AtualizarMinecraft(inscrição);

            return View("Sucesso");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{controller}/{action}/{id}")]
        public IActionResult DeferirHackathon(int id)
        {
            var inscrição = HackathonPorId(id);
            Deferir(inscrição);
            AtualizarHackathon(inscrição);

            return View("Sucesso");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult IndeferirHackathon(int id)
        {
            var inscrição = HackathonPorId(id);
            Indeferir(inscrição);
            AtualizarHackathon(inscrição);

            return View("Sucesso");
        }

        private void Deferir(IInscriçãoModelo<CadastroModelo> inscrição)
        {
            inscrição.Análise = false;
            inscrição.Aprovada = true;
        }

        private void Indeferir(IInscriçãoModelo<CadastroModelo> inscrição)
        {
            inscrição.Análise = false;
            inscrição.Aprovada = false;
        }

        private InscriçãoMinecraftModelo MinecraftPorId(int id) => _dadosMinecraft.Where(m => m.Id == id).SingleOrDefault();
        private InscriçãoHackathonModelo HackathonPorId(int id) => _dadosHackathon.Where(h => h.Id == id).SingleOrDefault();

        private void AtualizarMinecraft(InscriçãoMinecraftModelo inscrição)
        {
            _dadosMinecraft.Update(inscrição);
            _contexto.SaveChanges();
        }

        private void AtualizarHackathon(InscriçãoHackathonModelo hackathon)
        {
            _dadosHackathon.Update(hackathon);
            _contexto.SaveChanges();
        }

    }
}
