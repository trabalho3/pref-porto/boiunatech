﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infraestrutura;
using Infraestrutura.Geradores.Interfaces;
using Infraestrutura.Modelos;
using Infraestrutura.Modelos.Interfaces;
using Infraestrutura.Serviços.Interfaces;
using Infraestrutura.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BoiunaSite.Controllers
{
    public class AdminController : Controller
    {
        private readonly Contexto _contexto;
        private readonly DbSet<LoginModelo> _dadosLogin;
        private readonly IDadosValidaçãoGerador _geradorDadosValidação;
        private readonly ICriptografiaServiço _criptografiaServiço;

        public AdminController(Contexto contexto, IDadosValidaçãoGerador geradorDadosValidação, ICriptografiaServiço criptografiaServiço)
        {
            _contexto = contexto;
            _dadosLogin = _contexto.Set<LoginModelo>();
            _geradorDadosValidação = geradorDadosValidação;
            _criptografiaServiço = criptografiaServiço;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin")) return RedirectToAction(nameof(Painel));

            return View(new AdminViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logar(AdminViewModel viewModel)
        {
            var senhaCriptografada = _criptografiaServiço.Criptografar(viewModel.Senha);
            var modelo = _dadosLogin.Where(l => l.Login == viewModel.Login && l.Senha == senhaCriptografada).SingleOrDefault();
            if (modelo != null)
            {
                Logar(modelo);
                viewModel.Nome = modelo.Nome;
                viewModel.Senha = "";
                return View("Painel", viewModel);
            }

            viewModel.Validação = _geradorDadosValidação.Gerar(true, "Usuário não encontrado");
            return View("Index", viewModel);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Painel()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Index));
        }

        private async void Logar(ILoginModelo modelo)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, modelo.Login),
                new Claim("Id", modelo.Id.ToString()),
                new Claim(ClaimTypes.Name, modelo.Nome),
                new Claim(ClaimTypes.Role, "Admin")
            };

            var identidadeDeUsuário = new ClaimsIdentity(claims, "Login");
            var claimsPrincipal = new ClaimsPrincipal(identidadeDeUsuário);
            var propriedades = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTime.Now.ToLocalTime().AddHours(2),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal, propriedades);
        }
    }
}
