﻿var elemento = $("#mensagem");
var classes = new Array("d-none", "alert-danger", "alert-success");

$("#verificar").click(function (evento)
{   
    let valor = $("#codigo").val();
    if (valor == "")
    {
        SetarMensagemAlerta("Código não pode ser vazio");
        evento.preventDefault();
    }
});

function SetarMensagemAlerta(mensagem)
{
    RemoverClassesMensagem();
    elemento.addClass(classes[1]);
    elemento.html(mensagem);
}

function RemoverClassesMensagem()
{
    classes.forEach(classe => {
        if (elemento.hasClass(classe)) elemento.removeClass(classe);
    }); 

}