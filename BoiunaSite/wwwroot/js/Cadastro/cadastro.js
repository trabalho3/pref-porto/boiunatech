﻿var errosId = "Erros";

function SetarConteudoErros(texto)
{
    $("#" + errosId).html(texto);
    $("#" + errosId).removeClass("d-none");
}

function MostrarErro(event) 
{
    event.preventDefault();
    document.getElementById("BannerSuperior").scrollIntoView();
}


$(document).ready(function () {
    $('#CPF').mask('000.000.000-00');
    $('#CPFResponsavel').mask('000.000.000-00');
    $('#Telefone').mask('(00) 00000-0000');
    $('#Nascimento').mask('00/00/0000');
    $('#CEP').mask('00000-000');
    $('#Agencia').mask('#');
    $('#Conta').mask('#');
});

$("#Cadastrar").click(function (event) {
    var ids = ["Nome", "CPF", "Nascimento", "Telefone", "Email", "Senha", "SenhaRepetir", "Endereco", "CEP", "Cidade", "RGCPF", "Casa", "NomeBanco", "Agencia", "Conta", "Banco", "Selfie", "SelfieResponsavel"];
    var nomes = ["Nome", "CPF", "Data de Nascimento", "Telefone", "E-mail", "Senha", "Confirmar Senha", "Endereço", "CEP", "Cidade", "RG e CPF", "Comprovande de Endereço",
        "Banco", "Agência", "Conta", "Comprovante Bancário", "Selfie com RG e CPF", "Selfie do Responsável com RG e CPF"];

    for (var i = 0; i < ids.length; i++)
    {
        if ($("#" + ids[i]).val() == "")
        {
            SetarConteudoErros("O campo " + nomes[i] + " não pode ser vazio.");
            MostrarErro(event);
            return;
        }
    }

    if ($("#Senha").val() != $("#SenhaRepetir").val())
    {
        SetarConteudoErros("As duas senhas não são iguais");
        MostrarErro(event);
        return;
    }

    if ($("#Senha").val().length < 6)
    {
        SetarConteudoErros("Senha deve ter pelo menos 6 caracteres");
        MostrarErro(event);
        return;
    }
});