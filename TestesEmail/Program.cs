﻿using Infraestrutura.DAO;
using Infraestrutura.Helpers;
using System;

namespace TestesEmail
{
    class Program
    {
        static void Main(string[] args)
        {
            var ambiente = new AmbienteDAODebug();
            var emailHelper = new EmailHelper(ambiente);

            emailHelper.EmailConfirmação("", "matheuslacerda.08@gmail.com");

            Console.WriteLine("Aperte ENTER....");
            Console.ReadKey();
        }
    }
}
